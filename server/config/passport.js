const LDAPStrategy = require('passport-ldapauth');
const JWTStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;
const { secret, ldap } = require('./auth.js');

module.exports = function (passport) {
  // JWT Configuration
  const jwtOptions = {
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey: secret
  };

  // Use passport with JWT strategy
  passport.use(new JWTStrategy(jwtOptions,
    function (payload, done) {
      console.log('payload received', payload);
      done(null, payload);
    }));

  // Use passport with the LDAP strategy
  const searchAttributes = ldap.searchAttributes.split(',').map(e => e.trim());
  const ldapOpts = { server: { ...ldap, searchAttributes } };
  passport.use(new LDAPStrategy(ldapOpts));
};
