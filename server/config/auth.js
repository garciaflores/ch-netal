const env = process.env.NODE_ENV || 'development';

const config = {
  production: {
    secret: process.env.JWT_SECRET || '',
    ldap: {
      url: process.env.LDAP_URL || '',
      bindDN: process.env.LDAP_ADMIN_USER || '',
      bindCredentials: process.env.LDAP_ADMIN_PWD || '',
      searchBase: process.env.LDAP_BASE || 'ou=people,dc=univ-paris13,dc=fr',
      searchFilter: process.env.LDAP_FILTER || '(&(uid={{username}})(accountStatus=active*))',
      searchAttributes: process.env.LDAP_ATTRIBUTES || 'uid, mail, displayName, accountStatus'
    }
  },
  development: {
    secret: process.env.DEV_JWT_SECRET || 'lipn-cat',
    ldap: {
      url: process.env.DEV_LDAP_URL || 'ldap://0.0.0.0:389',
      searchBase: process.env.DEV_LDAP_BASE || 'ou=people,dc=planetexpress,dc=com',
      searchFilter: process.env.DEV_LDAP_FILTER || '(uid={{username}})',
      searchAttributes: process.env.DEV_LDAP_ATTRIBUTES || 'uid, mail, cn',
      passwordField: 'userPassword'
    }
  },
  test: {
    secret: process.env.TEST_JWT_SECRET || 'lipn-cat'
  }
};

module.exports = config[env];
