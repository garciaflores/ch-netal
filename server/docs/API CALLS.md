# Step by Step API calls for a complete user experience

## Description of the use case

User will set as a corpus three articles of "La voix du Nord".
It will applies onto it POS Tagging, Neologisms detection with Neoveille & motifs detection with SDMC.

## I: User set their corpus.

POST /corpora

Request Body:
``` javascript
{
  documents: [
    {
      corpusId: null, // Pas encore créé du coup....
      source: '/article1.txt',
      conlluBuffer: null // TO BE FILLED,maybe later?
    },
    {
      corpusId: null, // Pas encore créé du coup....
      source: '/article2.txt',
      conlluBuffer: null // TO BE FILLED,maybe later?
    },
    {
      corpusId: null, // Pas encore créé du coup....
      source: '/article3.txt',
      conlluBuffer: null // TO BE FILLED,maybe later?
    }
  ],
  createdBy: 'myUserId',
  type: 'public',
  creationDate: '12/10/2020 - 15h44',
  metadata: {
    author: 'La Voix du Nord',
    title: 'Trois articles récents du journal « La voix du Nord »',
    description: "Ces trois articles sont pris au hasard et ont été récupéré grâce à l'outil SOLR de Néoveille. Ils servent comme dummy corpus",
    date: null,
    type: 'Article de journal',
    size: null,
    language: 'fr',
    userMetadata: []
  }
}
```
System will now maybe check all the informations, then try to fill conlluBuffer of each documents by tokenizing the corpus. It will also need to compute the size of the corpus.

Informations intéressantes:
- Nombre de mots / tokens
- Taille informatique (du fichier source)

From now on, this document (as in a mongo document) will bear the id "corpusId".

## II: User set their pipeline

PRETREATMENTS:
La segmentation par phrases et par mots se fera systématiquement lors du conllu buffer, on peut donc enlever ces propriétés de preTreatments.


POST /pipelines

Request body:
``` javascript
{
  preTreatments: {
    sentenceSegmentation: false,
    wordSegmentation: true,
    posTagger: true,
    conversionToUTF: false
  },
  // Processes TO BE FILLED
  processes: [
    {
      moduleName: 'Neoveille',
      moduleParameters: [
        {
          name:,
          value:
        }
      ]
    },
    {
      moduleName: 'SDMC',
      moduleParameters: [
        {
          name:,
          value:
        }
      ]
    }
  ],
  creationDate: '12/10/2020 - 15h52',
  description: "Cette chaîne de traitement permet d'identifier les néologismes ainsi que les motifs récurrents."
}
```

This pipeline, created as a document will now bear the id "pipelineId".

## III: System creates a corpus process and starts it

Now that the system registered both the corpus and the pipeline. It can create the corpus process linking those two and start it.

POST /corpusProcesses

Request body:
``` javascript
{
  corpusId: corpusId,
  pipelineId: pipelineId,
  userId: 'myUserId',
  conllu: null, // Will be computed after the POST
  visualAnnotatedDocuments: [], // It will be computed after the pipeline
  outputs: null,
  currentProcessModule: null,
  status: 'Not started yet'
}
```

On concatène tous les conlluBuffer de chaque document du corpus «corpusId» (GET /corpus/corpusId/...)

PUT /corpusProcesses/corpusProcessId
pour ajouter le conllu concaténer.

### Pre-Treatments executions

Once it is added to the collection, it will fetch the preTreatments in the document of the corresponding pipeline.

``` javascript
preTreatments: {
    //sentenceSegmentation: false,
    //wordSegmentation: true,
    posTagger: true,
    codeConversionToUTF: false
  }
```

wordSegmentation is supposed to be already done as we posted the corpus.

So the system will need to apply the POS Tagger to the corpus.

**TO BE FILLED**

```
I guess it needs to call /modules/treeTagger, 
fetch /corpora/corpusId or /corpusProcess/corpusProcessId and get its conllu which would be the concatenated version.
Then apply the process, then returning a conllu column that we can add to corpusProcess.conllu.
```

### First module execution

Once it is added to the collection, it will fetch the first process in the list of the corresponding pipelineId and call the module, then update:

> POST /modules/Neoveille

Request body:
``` javascript
{
  // TO BE FILLED
  moduleParameters: [
    {
      name:,
      value:
    }
  ]
}
```

### At the same time: update of corpusProcess

> PUT /corpusProcess/corpusProcessId

Request body:
``` javascript
{
  currentProcessingModule: 'Neoveille', // null -> Neoveille
  status: 'Started' // Not started yet -> Started
}
```

## IV: Module Processing

The module will need to fetch the conllu.

> GET /corpusProcesses/corpusProcessId/conllu

Then, given parameters given from the previous paragraph, applies the process to the conllu.

Neoveille will then give back two things:
- one output: the raw list of found neologisms
- one conllu column with

> POST /corpusProcess/corpusProcessId/processOutputs

Request body:
``` javascript
{
  newConlluColumn:{
    processId: 'neoveilleProcess',
    columnTitle: 'NEOLOGISMS',
    columnData: '_\n_\nY\n_' //Mockup
  },
  newOutput:{
    processId: 'neoveilleProcess',
    moduleName: 'Neoveille',
    content: {
      title: 'Liste des néologismes',
      description: 'Effectué par Néoveille 2.0, créé par ...., avec ces paramètres:....',
      data: 'schtroumpf\ncoronavirusé\nmacronisme' // ????
    }
  }
}
```

This request adds those two outputs of neoveille process to the collection corpusProcessId.

### When Neoveille is finised...

(Comment le système sait quand Neoveille finit ?)

We can start the next module on the pipeline list.

> POST /modules/SDMC

Request body:
``` javascript
{
  // TO BE FILLED
  moduleParameters: [
    {
      name:,
      value:
    }
  ]
}
```
Then update our corpusProcess

> PUT /corpusProcesses/corpusProcessId

Request body:
``` javascript
{
  currentProcessingModule: 'SDMC'
}
```

Now, similar to Neoveille, SDMC will need to fetch the conllu, 

> GET /corpusProcesses/corpusProcessId/conllu

Process that corpus, then adds its output to the structure

> POST /corpusProcesses/corpusProcessId/processOutputs

Request body:
``` javascript
{
  newConlluColumn: {
    processId: 'SDMCProcess',
    columnTitle: 'MOTIFS',
    columnData: '_\n_\n1:3\n_' //Mockup
  }
}
```

## V - Processing annotations

Now that our pipeline is finished, we can update our status

> PUT /corpusProcesses/corpusProcessId

Request body:
``` javascript
{
  currentProcessingModule: null,
  status: 'Processing annotations'
}
```
From now, we can start analysing the conllu and populate AnnotatedDocuments.

First, the system extrates the conllu.

> GET /corpusProcesses/corpusProcessId/conllu

Then it processes it and adds to annotated documents the annotations:

> POST /corpusProcesses/corpusProcessId/visualAnnotatedDocument

On coupe le conllu pour chaque document.
On traite pour chaque document toutes ses colonnes

Request body:
``` javascript
{
  newVisualAnotatedDocument:[
    {
      documentId: 'article1DocumentId',
      annotations: [
        {
          documentId: 'article1DocumentId',
          processId: 'NeoveilleProcess',
          conlluColumnId: 'conlluNeoveilleId',
          moduleName: 'Neoveille',
          content: {
            title: 'Annotation des Néologismes',
            description: 'Trouvés par Néoveille selon ces paramètres:',
            data: null // ??
          },
          color: null // computed later ?
        },
        {
          documentId: 'article1DocumentId',
          processId: 'SDMCProcess',
          conlluColumnId: 'conlluSDMCId',
          moduleName: 'Neoveille',
          content: {
            title: 'Annotation des Néologismes',
            description: 'Trouvés par Néoveille selon ces paramètres:',
            data: null // ??
          }
        }
      ]
    }
  ]
}
```

Il faudrait faire un calcul pour affecter les couleurs pour chaque annotation des documents.

Tout le process est terminé, on peut mettre à jour lestatus de corpusProcess

> PUT /corpusProcesses/corpusProcessId

Request body:
``` javascript
{
  status: 'Finished'
}
```

Et on envoie un mail à l'utilisateur s'il a activé l'option.


( POST /users/myUserId/sendMail )

