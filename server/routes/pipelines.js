var express = require('express');
var router = express.Router();

var pipelineController = require('../controllers/pipelineController.js');

// GET /pipelines
router.get('/', pipelineController.debug_pipelines_list)
// POST /pipelines
router.post('/', pipelineController.pipeline_create)
// GET /pipelines/:pipelineId
router.get('/:pipelineId', pipelineController.pipeline_get)
// DELETE /pipelines/:pipelineId
router.delete('/:pipelineId', pipelineController.pipeline_delete)

module.exports = router;
