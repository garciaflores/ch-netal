const express = require('express');
const jwt = require('jsonwebtoken');
const passport = require('passport');

const path = require('path');
const { secret } = require(path.join(__dirname, '/../config/auth.js'));

const router = express.Router();

/* Generates the JWT token */
function jwtToken (user) {
  return jwt.sign(user, secret);
}

/* POST LDAP login */
router.post('/',
  passport.authenticate('ldapauth', { session: false }),
  function (req, res) {
    // Successful authentication
    const token = jwtToken(req.user);

    res.json({
      user: req.user,
      token: token
    });
  });

module.exports = router;
