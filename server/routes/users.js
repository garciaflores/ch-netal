var express = require('express');
var router = express.Router();

var userController = require('../controllers/userController.js');


// DEBUG GET /users
router.get('/', userController.debug_user_list)

/* User management of personal informations */


// POST /users
router.post('/', userController.user_create)
// GET /users/:userId
router.get('/:userId', userController.user_get)
// PUT /users/:userId
router.put('/:userId', userController.user_modify)
// DELETE /users/:userId
router.delete('/:userId', userController.user_delete)

/* User management of profile corpora */

// GET /users/:userId/corpora
router.get('/:userId/corpora', userController.user_corpus_list)
// POST /users/:userId/corpora
router.post('/:userId/corpora', userController.user_corpus_create)
// GET /users/:userId/corpora/summary
router.get('/:userId/corpora/summary', userController.user_corpus_summary_list)
// GET /users/:userId/corpora/:corpusId
router.get('/:userId/corpora/:corpusId', userController.user_corpus_get)
// DELETE /users/:userId/corpora/:corpusId
router.delete('/:userId/corpora/:corpusId', userController.user_corpus_delete)

/* User management of pipelines */

// GET /users/:userId/pipelines
router.get('/:userId/pipelines', userController.user_pipeline_list)
// POST /users/:userId/pipeline
router.post('/:userId/pipelines', userController.user_pipeline_create)
// GET /users/:userId/pipelines/:pipelineId
router.get('/:userId/pipelines/:pipelineId', userController.user_pipeline_get)
// GET /users/:userId/pipelines/:pipelineId/status
router.get('/:userId/pipelines/:pipelineId/status', userController.user_pipeline_status)

module.exports = router;
