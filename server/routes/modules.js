var express = require('express');
var router = express.Router();

var moduleController = require('../controllers/moduleController.js');


// Fetch all modules
router.get('/', moduleController.module_list)
// Add a new module
router.post('/', moduleController.module_add)
// Fetch a module
router.get('/:moduleName', moduleController.module_get)
// Modify a module
router.put('/:moduleName', moduleController.module_modify)
// POST /modules/:moduleName
router.post('/:moduleName', moduleController.module_treatment_create)
// Delete a module given an ID
router.delete('/:moduleId', moduleController.module_delete);
// GET /modules/:moduleName/:pipelineId/status
router.get('/:moduleName/:pipelineId/status', moduleController.module_treatment_summary)

module.exports = router;
