var express = require('express');
var router = express.Router();

var corpusController = require('../controllers/corpusController.js');

// DEBUG: GET /corpora
router.get('/', corpusController.debug_corpus_list)

// POST /corpora
router.post('/', corpusController.corpus_create)
// POST /corpora/upload
router.post('/upload', corpusController.corpus_file_upload)
// GET /corpora/:corpusId
router.get('/:corpusId', corpusController.corpus_get)
// PUT /corpora/:corpusId
router.put('/:corpusId', corpusController.corpus_modify)
// DELETE /corpora/:corpusId
router.delete('/:corpusId', corpusController.corpus_delete)
// GET /corpora/:corpusId/summary
router.get('/:corpusId/summary', corpusController.corpus_summary)
// GET /corpora/:corpusId/documents
router.get('/:corpusId/documents', corpusController.corpus_documents_get)
// GET /corpora/:corpusId/:documentId
router.get('/:corpusId/:documentId', corpusController.corpus_document_get)
// PUT /corpora/:corpusId/:documentId
router.put('/:corpusId/:documentId', corpusController.corpus_document_modify)
// POST /corpora/:corpusId/:documentId
router.post('/:corpusId/:documentId', corpusController.corpus_document_basic_process_add)
// GET /corpora/:corpusId/:documentId/report
router.get('/:corpusId/:documentId/report', corpusController.corpus_document_report_get)

module.exports = router;