var express = require('express');
var router = express.Router();

var corpusProcessController = require('../controllers/corpusProcessController.js');

// DEBUG: GET /corpusProcesses
router.get('/', corpusProcessController.debug_corpus_process_list)

// Add a new corpus process
router.post('/', corpusProcessController.corpus_process_create)
// Fetch a corpus process
router.get('/:corpusProcessId', corpusProcessController.corpus_process_get)
// Modify a corpus process
router.put('/:corpusProcessId', corpusProcessController.corpus_process_modify)
// Delete a corpus process
router.delete('/:corpusProcessId', corpusProcessController.corpus_process_delete)
// Add a new process to a corpus process
router.post('/:corpusProcessId/process', corpusProcessController.corpus_process_add_process);
// Start a corpus process
router.post('/:corpusProcessId/start', corpusProcessController.corpus_process_start);
// Execute the next process in the pipeline
router.post('/:corpusProcessId/continue', corpusProcessController.corpus_process_continue);
// GET /corpusProcesses/:corpusProcessId/conllu
//router.get('/:corpusProcessId/conllu')
// Fetch the current status of a corpus process
router.get('/:corpusProcessId/status', corpusProcessController.corpus_process_status_get)
// Get all the corpus processes of a user
router.get('/:userId', corpusProcessController.corpus_process_user_get)


module.exports = router;