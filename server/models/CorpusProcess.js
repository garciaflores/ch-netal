// ***
// CorpusProcess Model file
// ***

const mongoose = require('mongoose');

const Schema = mongoose.Schema;

/**
 * @swagger
 * components:
 *  schemas:
 *    ProcessOutput:
 *      type: object
 *      description: data returned by a module at the end of a process
 *      required:
 *      - processId
 *      - moduleName
 *      - content.data
 *      properties:
 *        processId:
 *          type: String
 *          description: id of process that generated that output
 *        moduleName:
 *          type: String
 *          description: name of the module that generated that output
 *        content:
 *          type: object
 *          description: contains the information of the output
 *          properties:
 *            title:
 *              type: String
 *              description: a text given to describe the output
 *              example: Neologisms found by Neoveille
 *            description:
 *              type: String
 *              description: more text to detail the output
 *              example: 'This is the result of the module SDMC given those parameters: ....'
 *            data:
 *              type: String
 *              description: the content produced by the module
 *              example: macronisme\nadulescent\ncapilotracté
 */

// Output returned by a module
// Can be a text file or something else like a model.
/* Should there be documentId or corpusId? */
const ProcessOutputSchema = new Schema({
  processId: { type: String, required: false},
  moduleName: { type: String, required: true}, // Can be built from processId
  content: {
    title: { type: String },
    description: { type: String },
    data: { type: String, required: true}
  }
});

/**
 * @swagger
 * components:
 *  schemas:
 *    Annotation:
 *      type: object
 *      description: contains the annotation produced for a given process for a single document
 *      required:
 *      - documentId
 *      - processId
 *      - moduleName
 *      - content.data
 *      properties:
 *        documentId:
 *          type: String
 *          description: id of the document whose annotation is attached
 *        processId:
 *          type: String
 *          description: id of the process whose annotation is attached
 *        conlluColumnId:
 *          type: String
 *          description: id of the conllu column whose annotation is attached
 *        moduleName:
 *          type: String
 *          description: name of the module that produced this annotation
 *        content:
 *          type: object
 *          description: contains the information of the annotation
 *          properties:
 *            title:
 *              type: String
 *              description: a text given to describe the annotation
 *            description:
 *              type: String
 *              description: more text to detail the annotation
 *            data:
 *              type: String
 *              description: the actual annotations produced by a process
 *        color:
 *          type: String
 *          description: the color that will be used to highlight annotated tokens
 */

// An annotation is the conjunction of a module and a document.
// It must contains all the informations needed for the visualisation tool to work.
const AnnotationSchema = new Schema({
  documentId: { type: String, required: true}, // Needed ?
  processId: { type: String, required: true},
  conlluColumnId: { type: String, required: true}, // An annotation is also rattachedto a conlluColumn
  moduleName: { type: String, required: true}, // Can be built from processId
  content: {
    title: { type: String },
    description: { type: String },
    data: { type: String, required: true}
  },
  color: { type: String }    
})

/**
 * @swagger
 * components:
 *  schemas:
 *    ConlluColumn:
 *      type: object
 *      description: the content of a column following ConLLu-Plus conventions
 *      required:
 *      - columnTitle
 *      - columnData
 *      properties:
 *        processId:
 *          type: String
 *          description: id that produced that column
 *        columnTitle:
 *          type: String
 *          description: the text corresponding to the head of the column
 *          example: UPOS
 *        columnData:
 *          type: String
 *          description: the data corresponding to the conllu annotation, where each row is the annotation of a token
 *          example: ADV\nVERB\nPRON\nNUM\nNOUN\nPUNCT
 * 
 */

// The content of a column with ConLLU-Plus style, each row corresponding to a token.
const ConlluColumnSchema = new Schema({
  columnId: {type: String }, // Needed ?
  processId: { type: String, required: false}, // Not for base processing
  columnTitle: { type: String, required: true},
  columnData: { type: String, required: true}
})

/**
 * @swagger
 * components:
 *  schemas:
 *    ConlluCols:
 *      type: array
 *      items:
 *        $ref: "#/components/schemas/ConlluColumn"
 */
const ConlluColsSchema = new Schema({
  conlluColumns: [ConlluColumnSchema]
})

/**
 * @swagger
 * components:
 *  schemas:
 *    AnnotatedDocument:
 *      type: object
 *      description: the list of annotations attached to a document
 *      properties:
 *        documentId:
 *          type: String
 *          description: id of the document that has been annotated
 *        corpusProcessId:
 *          type: String
 *          description: id of the corpusProcess that has produced those annotations
 *        annotations:
 *          type: array
 *          description: list of annotations for the document
 *          items:
 *            $ref: "#/components/schemas/Annotation"
 */

// Represent the list of annotations attached to a document
const AnnotatedDocumentSchema = new Schema({
  documentId: { type: String},
  corpusProcessId: { type: String},
  annotations: { type: [AnnotationSchema]}
})

/**
 * @swagger
 * components:
 *  schemas:
 *    CorpusProcess:
 *      type: object
 *      description: Actual processing of a designated corpus with a pipeline
 *      properties:
 *        corpusId:
 *          type: String
 *          description: id of the corpus processed
 *        pipelineId:
 *          type: String
 *          description: id of the pipeline that processes the corpus
 *        userId:
 *          type: String
 *          description: id of the user that launched that process
 *        conllu:
 *          description: representation of the conllu file annotated by the pipeline
 *          $ref: "#/components/schemas/Conllu"
 *        annotatedDocuments:
 *          type: array
 *          description: list of annotations per document
 *          items:
 *            $ref: "#/components/schemas/AnnotatedDocument"
 *        processOutputs:
 *          type: array
 *          description: list of output produced by the pipeline
 *          items:
 *            $ref: "#/components/schemas/ProcessOutput"
 *        currentProcessingModule:
 *          type: String
 *          description: Name of the module that is being executed currently by the pipeline. Is null if the pipeline has not started, failed, or finished.
 *        status:
 *          type: String
 *          enum:
 *          - Not started yet
 *          - Started
 *          - Finished
 *          - Processing annotations
 *          - Failed
 *          description: Status of the pipeline
 */

const CorpusProcessSchema = new Schema({
  corpusId: { type: String, required: true},
  pipelineId: { type: String, required: true},
  userId: { type: String, required: true},
  
  //conlluCols: { type: ConlluColsSchema},
  conlluCols: [ConlluColumnSchema],
  
  // Annotation:
  // This is a bit awkward. What happens if you have 3 annotations for each of your 3 files?
  // Maybe it's better
  /*annotations: { type: [AnnotationSchema]},*/
  annotatedDocuments: { type: [AnnotatedDocumentSchema]},
  // The output that a module can return (describing the process that was done)
  processOutputs: { type: [ProcessOutputSchema]},
  // What module is currently processing the corpus?
  currentProcessingModule: { type: String }, 
  // Get the index of the current processing module
  currentProcessId: { type: Number},
  status: {
    type: String,
    enum: ['Not started yet', 'Started', 'Processing annotations', 'Finished', 'Failed']
  }
})

// Let's compile as a model
const CorpusProcessModel = mongoose.model('CorpusProcessModel', CorpusProcessSchema);


module.exports= {
  ConlluColumnSchema: ConlluColumnSchema,
  CorpusProcessModel: CorpusProcessModel
}