// ****
// Module Model file
// ***

const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const ParameterSchema = new Schema({
  name: String,
  // Need localization oflabels & description
  // Check plugin mongoose-i18n / mongoose-i18n-neutral
  label: String,
  description: String,
  type: { 
    type: String,
    enum: ['List-Mono', 'List-Poly', 'Int', 'Bool', 'String']
  },
  items: [String],
  default: Schema.Types.Mixed
  //default: String // If int-2 -> 2 elements, else -> 1 elements,maybe better to split int-2 into 2x int 
})

const ModuleSchema = new Schema({
  name: String,
  description: String, // need localization
  parameters: { type: [ParameterSchema] }
})

// Let's compile as a model
const ModuleModel = mongoose.model('ModuleModel', ModuleSchema);

module.exports = ModuleModel;
