// ***
// User Model file
// ***

const mongoose = require('mongoose');

const Schema = mongoose.Schema;

/**
 * @swagger
 * components:
 *  schemas:
 *    User:
 *      type: object
 *      description: Identity of an user
 *      properties:
 *        name:
 *          type: String
 *          description: user's name or nickname
 *          example: JohnDoe
 *        mail:
 *          type: String
 *          description: user's e-mail address
 *          example: john@doe.com
 *        password:
 *          type: String
 *          description: user's password (hashed?)
 *          example: mylittlepony42
 *        status:
 *          type: String
 *          enum:
 *          - Guest
 *          - User
 *          - Admin
 *          description: user's status
 *          example: User
 *        profileCorpora:
 *          type: array
 *          description: list of corpora added by the user
 *          items:
 *            type: String
 *        corpusProcesses:
 *          type: array
 *          description: list of pipelines executed by the user
 *          items:
 *            type: object
 *            properties:
 *              corpusProcessId:
 *                type: String
 *                description: id of a corpusProcess executed by the user
 *              status:
 *                type: String
 *                description: status of a corpusProcess executed by the user
 *        config:
 *          type: object
 *          description: diverse configurations of a user
 *          properties:
 *            sendMailWhenPipelineFinished:
 *              type: Boolean
 *              description: asserts if the user wants to receive a mail when a pipeline it executed have been finished.  
*/

// A user is composed of those attributes
const UserSchema = new Schema({
  name: { type: String},
  mail: { type: String},
  password: { type: String },
  status: { 
    type: String,
    enum: ['Guest', 'User','Admin']
  },
  profileCorpora: {type: [{ corpusId: String }]},
  corpusProcesses: { type: [{corpusProcessId: String, status: String}]},
  config: {
    sendMailWhenPipelineFinished: { type: Boolean, default: true}
  }
});

// Let's compile as a model
const UserModel = mongoose.model('UserModel', UserSchema);

module.exports = UserModel;
