// ****
// Pipeline Model file
// ***

const mongoose = require('mongoose');

const Schema = mongoose.Schema;

/**  
 * @swagger
 * components:
 *  schemas:
 *    Process:
 *      type: object
 *      description: Instructions for a module to execute given a list of parameters
 *      properties:
 *        moduleName:
 *          type: String
 *          description: Name of the module that execute the process
 *          example: SDMC
 *        moduleParameters:
 *          type: array
 *          description: A parameter for the module that execute the process
 *          example: [{ name: 'motif_type', value: 'Forme du mot seul'},{name: 'motif_representation', value: 'Fermés (sans sous-motifs de même fréquence)'}]
 *          items:
 *            type: object
 *            properties:
 *              name:
 *                type: String
 *                description: Name of the parameter
 *                example: motif_type
 *              value:
 *                type: String
 *                description: Value of the parameter
 *                example: Forme du mot seul
 */

// A process contains the information needed for a module to be executed
/* Should it contains corpusId? */
const ProcessSchema = new Schema({
  moduleName: { type: String, required: true},
  moduleParameters: {
    type:
    [{ 
      name: String,
      value: Schema.Types.Mixed
    }],
    required: true
  }
})

/**
 * @swagger
 * components:
 *  schemas:
 *    Pipeline:
 *      type: object
 *      description: A list of process to execute on a corpus
 *      properties:
 *        preProcessingOptions:
 *          type: object
 *          description: contains the information about the pre-processing for the pipeline
 *          properties:
 *            sentenceSegmentation:
 *              type: Boolean
 *              description: should the corpus be segmented into sentences?
 *            wordSegmentation:
 *              type: Boolean
 *              description: should the corpus be segmented into words?
 *            posTagger:
 *              type: Boolean
 *              description: should each token be anotated with its part-of-speech?
 *            codeConversionToUTF:
 *              type: Boolean
 *              description: should the corpus be converted to UTF?
 *        processes:
 *          type: array
 *          items:
 *           $ref: '#/components/schemas/Process'
 *        creationDate:
 *          type: Date
 *          description: The date where the corpus has been added to the database
 *        description:
 *          type: String
 *          description: Some text to describe the use of the pipeline
 */

// A pipeline contains the instruction to apply one or several
// modules to a corpus.
const PipelineSchema = new Schema({
  // CorpusId is no more in pipeline, a pipeline is only an array of instructions.
  /*corpusId: { type: String, required: true },*/
  preProcessingOptions: {
    required: true,
    type: {
      sentenceSegmentation: { type: Boolean, required: true },
      // wordSegmentation: Boolean,
      posTagger: { type: Boolean, required: true },
      codeConversionToUTF: { type: Boolean, required: true } // UTF-8
    }
  },
  // Moved in CorpusProcessing.js
  // currentProcessingModule: { type: String }, // What module is currently processing the corpus?
  // status: {
  //   type: String,
  //   enum: ['Not started yet', 'Started', 'Finished', 'Failed']
  // },
  processes: { type: [ProcessSchema] , required: true},
  creationDate: { type: Date },
  description: { type: String }
})

const PipelineModel = mongoose.model('PipelineModel', PipelineSchema);

module.exports = PipelineModel;