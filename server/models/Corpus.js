// ***
// Corpus Model file
// ***

// TODO: IMPLEMENT SUMMARY ( AS A VIRTUAL? )

const mongoose = require('mongoose');

const Schema = mongoose.Schema;
const { ConlluColumnSchema } = require('./CorpusProcess')

/**
 * @swagger
 * components:
 *  schemas:
 *    Document:
 *      type: object
 *      description: a part of a corpus, if the user chose multiple files
 *      properties:
 *        source:
 *          type: String
 *          description: the original text of the document, unannotated or path of the original file
 *        conlluBuffer:
 *          $ref: "#/components/schemas/ConlluColumn"
 *          description: tokenization of the corpus, so only the form.
 */

// Documents are build at the end of the pipeline
// It contains the source text and
// the annotations added by a pipeline
const DocumentSchema = new Schema({
  source: { type: String }, // path of the original document
  tokens: { type: ConlluColumnSchema },
  POStags: { type: ConlluColumnSchema },
  lemmas: { type: ConlluColumnSchema },
  lemmas_no_unknown: { type: ConlluColumnSchema },
  tokensIdx: { type: ConlluColumnSchema}  
})

/**
 * @swagger
 * components:
 *  schemas:
 *    Metadata:
 *      type: object
 *      description: contains informations about a corpus
 *      properties:
 *        author:
 *          type: String
 *          description: name of the entity that produced the corpus
 *          example:  Howard Phillips Lovecraft
 *        title:
 *          type: String
 *          description: title given to the corpus
 *          example: The Call of Cthulhu
 *        description:
 *          type: String
 *          description: more details about the corpus
 *          example: Full text of the book by LoveCraft scraped from a pdf.
 *        date:
 *          type: Date
 *          description: date of creation of the corpus
 *          example: 1928
 *        type:
 *          type: String
 *          description: categorization of the corpus
 *          example: short story
 *        size:
 *          type: String
 *          description: weight of the corpus in bytes
 *          example: 20Mb
 *        language:
 *          type: String
 *          description: language of the corpus, can be multilingual
 *          example: fr
 *        additionalMetadata:
 *          type: array
 *          description: list of metadata added by the user
 *          items:
 *            type: object
 *            description: pair of the name of a metadata and its value
 *            properties:
 *             name:
 *               type: String
 *               description: title of a metadata added by the user
 *             value:
 *               type: String
 *               description: value of a metadata added by the user     
 */


// Informations about a corpus
const MetadataSchema = new Schema({
  author: {type: String },
  title: {type: String },
  description: {type: String },
  date: {type: Date },
  type: {type: String },
  size: {type: String },
  language: {type: String },
  additionalMetadata: [{
    name: {type: String },
    value: {type: String }
  }]
})

/**
 * @swagger
 * components:
 *  schemas:
 *    Corpus:
 *      type: object
 *      description: a text that will be processed through a pipeline
 *      properties:
 *        documents:
 *          type: array
 *          description: list of documents that compose the corpus, used for the visualisation tool
 *          items:
 *            $ref: "#/components/schemas/Document"      
 *        createdBy:
 *          type: String
 *          description: '"userId" if created by a user or "public"'
 *        type:
 *          type: String
 *          description: whether the corpus is publicly available or private
 *          enum:
 *          - public
 *          - private
 *        creationDate:
 *          type: Date
 *          description: the date of creation of the corpus to the database
 *        metadata:
 *          $ref: "#/components/schemas/Metadata"
 */

// Representation of one or several documents
const CorpusSchema = new Schema({
  documents: { type: [DocumentSchema]},
  createdBy: { type: String}, // userId
  type: {
    type: String,
    enum:['public', 'private']
  },
  creationDate : { type: Date },
  metadata: { type: MetadataSchema },
});

CorpusSchema.virtual('numberOfDocuments').get(function() {
  if (this.documents) {
    return this.documents.length;
  }
  else {
    return 0;
  }
});

// Let's compile as a model
const CorpusModel = mongoose.model('CorpusModel', CorpusSchema);

module.exports = CorpusModel;