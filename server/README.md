# CheneTAL

## Install dependencies

`npm install`

## Set up MongoDB

Make sure to have a mongo instance running.

For now it is not automatic that the modules are added to the database.
So you will need to POST each of the needed modules.

For that, you will need to do the following commands:


`cd files`

Replace _file.json_ by the module file 
`curl -X POST -H "Content-Type: application/json" -d @file.json http://localhost:8081/modules`

You can check if that worked if you have in the frontend the list of modules in your frontend in pipelines.

## Start server

`npm start`

If you are developping this repository, you can use the following command so that it automatically reloads when you modify a file.

`npm run devstart`

