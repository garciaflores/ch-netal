// ***
// User Controller file
// ***

const UserModel = require('../models/User.js');

const responseHelper = require('./lib/responseHelper');
const errorHandler = responseHelper.errorHandler;
const successHandler = responseHelper.successHandler;
const failHandler = responseHelper.failHandler; // For eg. "User not found" is a fail, not an error

const corpusHelper = require('./lib/corpusHelper');

/* User personal information management functions */

// DEBUG: User list
exports.debug_user_list = function(req, res) {
  UserModel.find({}, (error, users) => {
    if (error) { errorHandler(res, error); }
    else {
      successHandler(res, { users: users } );
    }
  })
}

// Create a new user
exports.user_create = function(req,res) {
  const newUser = UserModel({
    name: req.body.name,
    mail: req.body.mail,
    status: req.body.status
  });
  newUser.save((error, user) => {
    if (error) { errorHandler(res, error); }
    else {
      successHandler(res, { user: user}, 201);
    }
  })
}
// Fetch user informations about an existing user
exports.user_get = function(req,res) {
  const userId = req.params.userId;
  UserModel.findById(userId, (error, user) => {
    if (error) { errorHandler(res, error); }
    else if (!user) {
      failHandler(res, { user: `No user was found with id: ${userId}`}, 404);
    }
    else {
      successHandler(res, { user: user});
    }
  })
}
// Change informations on an existing user
exports.user_modify = function(req,res) {
  const userId = req.params.userId;
  // {new: true} lets you have the updated document as user.
  UserModel.findByIdAndUpdate(userId, req.body, {new: true}, (error, user) => {
    if (error) { errorHandler(res, error); }
    else if (!user) {
      failHandler(res, { user: `No user was found with id: ${userId}`}, 404);
    }
    else {
      successHandler(res, { user: user});
    }
  })
}
// Delete an existing user
exports.user_delete = function(req,res) {
  const userId = req.params.userId;
  UserModel.findByIdAndDelete(userId, (error, user) => {
    if (error) { errorHandler(res, error); }
    else if (!user) {
      failHandler(res, { user: `No user was found with id: ${userId}`}, 404);
    }
    else {
      successHandler(res, { user: null});
    }
  })
}

/* User profile corpora management functions */

// Fetch all the profile corpora of an existing user
exports.user_corpus_list = function(req,res) {
  // TODO: Fetch the corpora from the corpus DB
  const userId = req.params.userId;
   UserModel.findById(userId, (error, user) => {
    if (error) { errorHandler(res, error); }
    else if (!user) {
      failHandler(res, { user: `No user was found with id: ${userId}`}, 404);
    }
    else {
      // ProfileCorpora is a list of ids.
      corpusHelper.getCorporaListFromIds(user.profileCorpora, (error, profileCorporaList) => {
        if (error) {
          errorHandler(res, error);
        }
        else {
          successHandler(res, { profileCorpora: profileCorporaList });
        }
      })
    }
  })
}

// Fetch all the summaries of the profile corpora of an existing user
exports.user_corpus_summary_list2 = function(req,res) {
  // TODO: Fetch the corpora from the corpus DB
  const userId = req.params.userId;
   UserModel.findById(userId, (error, user) => {
    if (error) { errorHandler(res, error); }
    else if (!user) {
      failHandler(res, { user: `No user was found with id: ${userId}`}, 404);
    }
    else {
      // ProfileCorpora is a list of ids.
      corpusHelper.getCorporaSummaryListFromIds(user.profileCorpora, (error, profileCorporaSummaryList) => {
        if (error) {
          errorHandler(res, error);
        }
        else {
          successHandler(res, { profileCorpora: profileCorporaSummaryList });
        }
      })
    }
  })
}

// Fetch all the summaries of the profile corpora of an existing user
exports.user_corpus_summary_list = async function(req,res) {
  // TODO: Fetch the corpora from the corpus DB
  const userId = req.params.userId;
  try {
    const user = await UserModel.findById(userId);
    if (!user) {
      failHandler(res, { user: `No user was found with id: ${userId}`}, 404);
    }
    else {
      // ProfileCorpora is a list of ids.
      //const profileCorporaSummaryList = await corpusHelper.getCorporaSummaryListFromIds(user.profileCorpora);
      //console.log("Appel successHandler (normalement après profileCorporaSummaryList");
      //await successHandler(res, { profileCorpora: profileCorporaSummaryList });
      corpusHelper.getCorporaListFromIds(user.profileCorpora, (error, profileCorporaSummaryList) => {
        if (error) {
          errorHandler(res, error);
        }
        else {
          successHandler(res, { profileCorpora: profileCorporaSummaryList });
        }
      });
    }
  }
  catch(error) {
    errorHandler(res, error);
  }
}

// Add a new corpus to an existing user
exports.user_corpus_create = function(req,res) {
  const userId = req.params.userId;
  UserModel.findById(userId, (error, user) => {
    if (error) { errorHandler(res, error); }
    else if (!user) {
      failHandler(res, { user: `No user was found with id: ${userId}`}, 404);
    }
    else {
      const corpusId = req.body.corpusId;
      
      // const newCorpus = { corpusId: corpusId};
      
      user.profileCorpora.push(corpusId);
      user.save((error, _) => {
        if (error) { errorHandler(res, error); }
        else {
          successHandler(res, { profileCorpus: newCorpus}, 201);
        }
      })
    }
  })
}
// Fetch a corpus for an existing user
exports.user_corpus_get = function(req, res) {
  const userId = req.params.userId;
  UserModel.findById(userId, (error, user) => {
    if (error) { errorHandler(res, error); }
    else if (!user) {
      failHandler(res, { user: `No user was found with id: ${userId}`}, 404);
    }
    else {
      const corpusId = req.params.corpusId;
      
      var profileCorpora = user.profileCorpora
      const corpusIndex = profileCorpora.findIndex((element) => {
        return element.corpusId === corpusId;
      });
      // Not found
      if (corpusIndex == -1) {
        failHandler(res, { profileCorpus: `No corpus with id: ${corpusId} was found among this user's profile corpora.`}, 404);
      }
      else {
        successHandler(res, { profileCorpus: profileCorpora[corpusIndex]});
      }
    }
  })
}
// Delete a corpus for an existing user
exports.user_corpus_delete = function(req,res) {
  const userId = req.params.userId;
  UserModel.findById(userId, (error, user) => {
    if (error) { errorHandler(res, error); }
    else if (!user) {
      failHandler(res, { user: `No user was found with id: ${userId}`}, 404);
    }
    else {
      const corpusId = req.params.corpusId;
      
      var profileCorpora = user.profileCorpora;
      const corpusIndex = profileCorpora.findIndex((element) => {
        return element.corpusId === corpusId;
      });
      // Not found
      if (corpusIndex == -1) {
        failHandler(res, { profilecoRPUS: `No corpus with id: ${corpusId} was found among this user's profile corpora.`}, 404);
      }
      else {
        profileCorpora.splice(corpusIndex, 1);
        user.save((error, _) => {
          if (error) { errorHandler(res, error); }
          else { successHandler(res, { profileCorpus: null}); }
        })
      }
    }
  })
}

/* User pipelines management functions */

// Fetch all pipelines from an existing user
exports.user_pipeline_list = function(req,res) {
  const userId = req.params.userId;
  UserModel.findById(userId, (error, user) => {
    if (error) { errorHandler(res, error); }
    else if (!user) {
      failHandler(res, { user: `No user was found with id: ${userId}`}, 404);
    }
    else {
      // Should fetch /pipelines ?
      successHandler(res,{ pipelines: user.pipelines });
    }
  })
}
// Add a new pipeline to an existing user
exports.user_pipeline_create = function(req,res) {
  const userId = req.params.userId;
  UserModel.findById(userId, (error, user) => {
    if (error) { errorHandler(res, error); }
    else if (!user) {
      failHandler(res, { user: `No user was found with id: ${userId}`}, 404);
    }
    else {
      const pipelineId = req.body.pipelineId;
      const newPipeline = { pipelineId: pipelineId };

      user.pipelines.push(newPipeline);
      user.save((error, _) => {
        if (error) { errorHandler(res, error); }
        else {
          successHandler(res, { pipeline: newPipeline}, 201);
        }
      })
    }
  })
}
// Fetch a specific pipeline from an existing user
exports.user_pipeline_get = function(req,res) {
  const userId = req.params.userId;
  UserModel.findById(userId, (error, user) => {
    if (error) { errorHandler(res, error); }
    else if (!user) {
      failHandler(res, { user: `No user was found with id: ${userId}`}, 404);
    }
    else {
      const pipelineId = req.body.pipelineId;

      var pipelines = user.pipelines;
      const pipelineIndex = pipelines.findIndex((element) => {
        return element.pipelineId === pipelineId;
      });
      // Not found
      if (pipelineIndex == -1) {
        failHandler(res, { pipeline: `No pipeline with id: ${pipelineId} was found among this user's pipelines`}, 404);
      }
      else {
        successHandler(res, { pipeline: pipelines[pipelineIndex]});
      }
    }
  })
}
// Get the status of a specific pipeline from an existing user
exports.user_pipeline_status = function(req,res) {
  const userId = req.params.userId;
  UserModel.findById(userId, (error, user) => {
    if (error) { errorHandler(res, error); }
    else if (!user) {
      failHandler(res, { user: `No user was found with id: ${userId}`}, 404);
    }
    else {
      const pipelineId = req.body.pipelineId;

      var pipelines = user.pipelines;
      const pipelineIndex = pipelines.findIndex((element) => {
        return element.pipelineId === pipelineId;
      });
      // Not found
      if (pipelineIndex == -1) {
        failHandler(res, { pipeline: `No pipeline with id: ${pipelineId} was foundamong this user's pipelines`}, 404);
      }
      else {
        var pipeline = pipelines[pipelineIndex];
        if (pipeline.status) {
          successHandler(res, { pipeline: {status: pipeline.status } });
        }
        else {
          failHandler(res, { pipeline: { status: `No status found for pipeline with id: ${pipelineId}`}});
        }
      }
    }
  })
}