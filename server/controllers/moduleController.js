// ***
// Module Controller file
// ***

const ModuleModel = require('../models/Module.js');

const responseHelper = require('./lib/responseHelper');
const errorHandler = responseHelper.errorHandler;
const successHandler = responseHelper.successHandler;
const failHandler = responseHelper.failHandler; // For eg. "User not found" is a fail, not an error

const moduleHelper = require('./lib/moduleHelper');

// Get all available modules and their specification
exports.module_list = async function(req,res) {
  // Js / Json file or in a collection ?....
  try {
    const modules = await ModuleModel.find({});
    successHandler(res, { modules });
  }
  catch (error) 
  { 
    errorHandler(res, error); 
  }
}

// Add a new module
exports.module_add = async function(req, res) {
  const newModule = ModuleModel({
    name: req.body.name,
    parameters: req.body.parameters,
    description: req.body.description
  });
  try {
    await newModule.save();
    successHandler(res, { newModule }, 201);
  }
  catch (error) {
    errorHandler(res, error);
  }
}

// Fetch a module given its name
exports.module_get = async function(req, res) {
  const moduleName = req.params.moduleName;
  try {
    const module = await ModuleModel.findOne({ name: moduleName });
    if (!module) {
      errorHandler(res, `No module was found with the name: ${moduleName}`, 404);
    }
    else {
      successHandler(res, { module });
    }
  }
  catch (error) {
    errorHandler(res, error);
  }
}

// Change the information of a module given its name
exports.module_modify = async function(req, res) {
  const moduleName = req.params.moduleName;
  try {
    const module = await ModuleModel.findOneAndUpdate({ name: moduleName}, req.body);
    if (!module) {
      errorHandler(res, `module_modify: No module was found with this name: ${moduleName}`);
    }
    else {
      successHandler(res, { module });
    }
  }
  catch (error) {
    errorHandler(res, error.message);
  }
}

// Delete a module given its id
exports.module_delete = async function (req, res) {
  const moduleId = req.params.moduleId;
  try {
    const module = await ModuleModel.findByIdAndDelete(moduleId);
    if (!module) {
      errorHandler(res, `No module was found with the name: ${moduleNAme}`,404);
    }
    else {
      successHandler(res, { module });
    }
  }
  catch (error) {
    errorHandler(res, error);
  }
}

// Create a new treatment for a module given some parameters
exports.module_treatment_create = function(req,res) {
  res.send(`NOT IMPLEMENTED! POST - Create a new treatment for ${req.params.moduleName} with params ${req.params}`)
}
// Get the status of a module given a pipeline Id
exports.module_treatment_summary = function(req,res) {
  res.send(`NOT IMPLEMENTED! GET - Get the status of the treatment for ${req.params.moduleName} for pipeline Id: ${req.params.pipelineId}`)
}