// ***
// Corpus Process Controller file
// ***

const { CorpusProcessModel } = require('../models/CorpusProcess.js');
const PipelineModel = require('../models/Pipeline.js');

const CorpusProcess = require('../models/CorpusProcess.js');
const { errorHandler, successHandler } = require('./lib/responseHelper');
const moduleHelper = require('./lib/moduleHelper.js');

// DEBUG: find all corpus processes
exports.debug_corpus_process_list = function (req, res) {
  CorpusProcessModel.find({}, (error, corpusProcesses) => {
    if (error) { errorHandler(res, error); }
    else {
      successHandler(res, { corpusProcesses: corpusProcesses });
    }
  })
}
// Add a new corpus process
exports.corpus_process_create = function (req, res) {
  const newCorpusProcess = CorpusProcessModel({
    corpusId: req.body.corpusId,
    pipelineId: req.body.pipelineId,
    userId: req.body.userId,
    status: 'Not started yet'
  });
  newCorpusProcess.save((error, corpusProcess) => {
    if (error) { errorHandler(res, error); }
    else {
      successHandler(res, { corpusProcess: corpusProcess }, 201);
    }
  })
}

// Get an existing corpus process
exports.corpus_process_get = async function (req, res) {
  const corpusProcessId = req.params.corpusProcessId;
  try {
    const corpusProcess = await CorpusProcessModel.findById(corpusProcessId);
    if (!corpusProcess) {
      errorHandler(res, `No corpus process was found with id: ${corpusProcessId}`, 404);
    }
    else {
      successHandler(res, { corpusProcess });
    }
  }
  catch (error) {
    errorHandler(res, error);
  }
}

// Modify an existing corpus
exports.corpus_process_modify = function (req, res) {
  const corpusProcessId = req.params.corpusProcessId;
  CorpusProcessModel.findByIdAndUpdate(corpusProcessId, req.body, (error, corpusProcess) => {
    if (error) { errorHandler(res, error); }
    if (!corpusProcess) {
      failHandler(res, { corpusProcess: `No corpus process was found with id: ${corpusProcessId}` }, 404);
    }
    else {
      successHandler(res, { corpusProcess: corpusProcess });
    }
  })
}
// Delete an existing corpus
exports.corpus_process_delete = function (req, res) {
  const corpusProcessId = req.params.corpusProcessId;
  CorpusProcessModel.findByIdAndDelete(corpusProcessId, (error, corpusProcess) => {
    if (error) { errorHandler(res, error); }
    if (!corpusProcess) {
      failHandler(res, { corpusProcess: `No corpus process was found with id: ${corpusProcessId}` }, 404);
    }
    else {
      successHandler(res, { corpusProcess: null })
    }
  });
}
// Get the conllu of an existing corpus process
exports.corpus_process_conllu_get = function (res, req) {
  const corpusProcessId = req.params.corpusProcesId;
  CorpusProcessModel.findById(corpusProcessId, (error, corpusProcess) => {
    if (error) { errorHandler(res, error); }
    if (!corpusProcess) {
      failHandler(res, { corpusProcess: `No corpus process was found with id: ${corpusProcessId}` }, 404);
    }
    else {
      // TO BE FILLED
      successHandler(res, { corpusProcess: { conllu: 'TBD' } });
    }
  })
}
// Get the status of an existing corpus process
exports.corpus_process_status_get = function (req, res) {
  const corpusProcessId = req.params.corpusProcessId;
  CorpusProcessModel.findById(corpusProcessId, (error, corpusProcess) => {
    if (error) { errorHandler(res, error); }
    if (!corpusProcess) {
      failHandler(res, { corpusProcess: `No corpus process was found with id: ${corpusProcesId}` }, 404)
    }
    else {
      //TO BE FILLED
      successHandler(res, { corpusProcess: { status: 'TBD' } });
    }
  })
}

// Get all corpus processes from a user
exports.corpus_process_user_get = function (req, res) {
  const userId = req.params.userId;
  CorpusProcessModel.find({ userId: userId }, (error, corpusProcesses) => {
    if (error) { errorHandler(res, error); }
    if (!corpusProcesses) {
      failHandler(res, { corpus: `No corpus processes were found with id: ${corpusProcessId}` }, 404);
    }
    else {
      successHandler(res, { corpusProcesses: corpusProcesses })
    }
  })
}

// Add a new conllu col and possibly an output from a module
exports.corpus_process_add_process = async function (req, res) {
  const corpusProcessId = req.params.corpusProcessId;
  try {
    let corpusProcess = await CorpusProcessModel.findById(corpusProcessId);
    if (!corpusProcess) {
      errorHandler(res, `No corpus processes were found with id: ${corpusProcessId}`, 404);
    }
    else {
      // Add conlluCol
      if (corpusProcess.conlluCols && corpusProcess.conlluCols.length > 0) {
        corpusProcess.conlluCols.push(req.body.conlluCol);
      }
      else {
        corpusProcess.conlluCols = [req.body.conlluCol];
      }

      // Add processOutput if there is one
      if (req.body.processOutput) {
         if (corpusProcess.processOutputs && corpusProcess.processOutputs.length > 0) {
        corpusProcess.processOutputs.push(req.body.processOutput);
        }
        else {
          corpusProcess.processOutputs = [req.body.processOutput];
        }
      }
      
      corpusProcess = await corpusProcess.save();
      successHandler(res, { corpusProcess });
    }
  }
  catch (error) {
    errorHandler(res, error);
  }
}

exports.corpus_process_start_old = async function (req, res) {
  const corpusProcessId = req.params.corpusProcessId;
  let corpusProcess;
  let started = false;
  let pipeline;

  try {
    corpusProcess = await CorpusProcessModel.findById(corpusProcessId);
    corpusProcess.status = 'Started';
    await corpusProcess.save();
    
    pipeline = await PipelineModel.findById(corpusProcess.pipelineId);
    started = true;
    successHandler(res, { message: "Pipeline started" });
  }
  catch (error) {
    errorHandler(res, error);
  }
  if (started) {
    try {
      // First POSTag
      await moduleHelper.startTreeTagger(corpusProcessId);
      for (let i = 0; i < pipeline.processes.length ; i++) {
        let process = pipeline.processes[i];
        console.log(`Starting module ${process.moduleName} for corpus process id: ${corpusProcessId}`);
        try {
          corpusProcess.currentProcessingModule = process.moduleName;
          await corpusProcess.save();
        }
        catch (error) {
          throw Error("Could not save current processing module");
        }
        await moduleHelper.startModuleProcess(corpusProcessId, process);
      }
      // Export
      await moduleHelper.startModuleProcess(corpusProcessId, { moduleName: 'Exporter'})
      console.log("Supposed to have finished the pipeline!");
      corpusProcess.currentProcessingModule = undefined;
      corpusProcess.status = 'Finished';
      await corpusProcess.save();
    }
    catch (error) {
      console.error(error);
    }
  }
}

exports.corpus_process_start = async function (req, res) {
  const corpusProcessId = req.params.corpusProcessId;
  let corpusProcess;
  let started = false;
  let pipeline;

  try {
    corpusProcess = await CorpusProcessModel.findById(corpusProcessId);
    corpusProcess.status = 'Started';
    // Easier for debugging (you can just call that endpoint to restart the pipeline)
    corpusProcess.currentProcessId = undefined;
    await corpusProcess.save();
    
    pipeline = await PipelineModel.findById(corpusProcess.pipelineId);
    started = true;
    successHandler(res, { message: "Pipeline started" });

    // First of all, let's tree tag our corpus
    moduleHelper.startTreeTagger(corpusProcessId);

    // When it is finished, it will call corpus_process_continue

  }
  catch (error) {
    errorHandler(res, error);
  }
}

exports.corpus_process_continue = async function (req, res) {
  const corpusProcessId = req.params.corpusProcessId;
  
  try {
    let corpusProcess = await CorpusProcessModel.findById(corpusProcessId);
    const pipeline = await PipelineModel.findById(corpusProcess.pipelineId);

    // If it's not started, start with zero, else, increment
    if (corpusProcess.currentProcessId == undefined) {
      corpusProcess.currentProcessId = 0;
    }
    else {
      corpusProcess.currentProcessId += 1;
    }

    // End of the pipeline!
    if (pipeline.processes.length <= corpusProcess.currentProcessId) {
      corpusProcess.currentProcessingModule = 'Exporter';
      await corpusProcess.save();

      successHandler(res, 'Let\'s export the conllu file');
      console.log(`Starting to export CONLLU for corpus process id: ${corpusProcessId}`);
      // Export the pipeline to a conllu file
      moduleHelper.startModuleProcess(corpusProcessId, { moduleName: 'Exporter'});

      // Should we do another endpoint 'finish'?
      corpusProcess.currentProcessingModule = null;
      corpusProcess.status = 'Finished';
      await corpusProcess.save();

    }
    else {
      let process = pipeline.processes[corpusProcess.currentProcessId]
      corpusProcess.currentProcessingModule = process.moduleName;
      await corpusProcess.save();
      console.log(`Starting module ${process.moduleName} for corpus process id: ${corpusProcessId}`);

      moduleHelper.startModuleProcess(corpusProcessId, process);

      successHandler(res, `Module ${process.moduleName} has started`);
    }
  }
  catch (error) {
    console.error(error);
  }
}


