const axios = require('axios');

exports.moduleList = [
  {
    moduleName: "Néoveille",
    moduleParameters: [
      {
        parameterName: "...",
        parameterValue: '...'
      }
    ]
  }
]

function nameToRoute(moduleName) {
  const routeDict = {
    "tree-tagger": "http://localhost:3000",
    "Exporter": "http://localhost:3001",
    "Modulix": "http://localhost:3002",
    "SDMC": "http://localhost:3003",
    "Néoveille": "http://localhost:3004",
    "Morfetik - Formes Simples": "http://localhost:3005"
  }
  return routeDict[moduleName];
}

exports.startTreeTagger = async function (corpusProcessId) {
  const moduleRoute = nameToRoute('tree-tagger');
  try {
    console.log(`Start tree-tagger for corpusProcess id: ${corpusProcessId}`);
    const res = await axios.post(moduleRoute, { corpusProcessId });
    console.log("Done with tree-tagger!");
  }
  catch (error) {
    throw error;
  }
}

exports.startModuleProcess = async function (corpusProcessId, process) {
  const moduleRoute = nameToRoute(process.moduleName);
  const reqParams = {
    corpusProcessId,
    parameters: process.moduleParameters
  }
  try {
    console.log(`Start module ${process.moduleName} for corpus process id: ${corpusProcessId}`);
    const res = await axios.post(moduleRoute, reqParams);
    console.log(`Done with module ${process.moduleName}!`);
  }
  catch (error) {
    throw Error(error.response.data.message);
  }
}
