const CorpusModel = require('../../models/Corpus.js');
const { errorHandler, failHandler } = require('./responseHelper.js');


// TEST: Unknown id -> it should be 404 and no goes to successhandler of userController (corpora list)
function getSummaryFromId(res, corpusId) {
  CorpusModel.findById(corpusId, (error, corpus) => {
    if (error) { errorHandler(res, error); }
    if (!corpus) {
      failHandler(res, { corpus: `No corpus was found with id: ${corpusId}`}, 404)
    }
    else {
      // Almost the same than the model, except we only get the number of docs, and not its content.
      const corpusSummary = {
        corpusId: corpusId,
        createdBy: corpus.createdBy,
        creationDate: corpus.creationDate,
        type: corpus.type,
        documentNumber: corpus.documents.length,
        metadata: corpus.metadata
      };
      return corpusSummary;
    }
  })
}

// Fetch all profile corpora from a user and returns their summaries
// Need number of documents
// cb is (error, corpusSUmmaryList)
exports.getCorporaSummaryListFromIds2 = function ( corpusIdList, cb) {
  CorpusModel.find( {'_id': {$in: corpusIdList}}, 'createdBy creationDate type metadata', (error, corporaSummaryList) => {
    if (error) {
      cb(error);
    }
    // No corpora were found at all
    if (!corporaSummaryList) {
      // to be filled
      //failHandler(res )
      // TODO: THROW ERROR
      pass
    }
    // Some of the corpora were not found
    if (corporaSummaryList.length < corpusIdList.length) {
      // To be filled
      pass
    }
    else {
      cb(null, corporaSummaryList);
    }
  })
}

exports.getCorporaSummaryListFromIds = function ( corpusIdList, cb) {
  CorpusModel.find( {'_id': {$in: corpusIdList}}, 'createdBy creationDate type metadata', (error, corporaSummaryList) => {
    if (error) {
      cb(error);
    }
    // No corpora were found at all
    if (!corporaSummaryList) {
      // to be filled
      //failHandler(res )
      // TODO: THROW ERROR
      cb('fail: no corpora found')
    }
    // Some of the corpora were not found
    if (corporaSummaryList.length < corpusIdList.length) {
      // To be filled
      cb('fail: some corpora were not found')
    }
    else {
      console.log("Return de corporaSummaryList");
      cb(null, corporaSummaryList);
    }
  })
}


// TESTS: Error, no corpus found, corporalist smaller than input list, success
exports.getCorporaListFromIds = function ( corpusIdList, cb) {
  CorpusModel.find( {'_id': {$in: corpusIdList}}, (error, corporaList) => {
    if (error) {
      cb(error);
    }
    // No corpora were found at all
    if (!corporaList) {
      // to be filled
      //failHandler(res )
      // TODO: THROW ERROR
      pass
    }
    // Some of the corpora were not found
    if (corporaList.length < corpusIdList.length) {
      // To be filled
      pass
    }
    else {
      cb(null, corporaList);
    }
  })
}