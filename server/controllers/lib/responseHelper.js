exports.errorHandler = function(res, error, httpCode = 500) {
    res.status(httpCode);
    res.json({
        status: "error",
        message: error
    });
    console.error(error);
}
exports.successHandler = function(res, data, httpCode = 200) {
    res.status(httpCode);
    res.json({
        status: "success",
        data: data
    });
}

// It might just be a pain to use that.
// I am just going to use errorHandler with a custom message instead
// For eg. "User not found" is a fail, not an error
exports.failHandler = function(res, data, httpCode) {
    res.status(httpCode);
    res.json({
        status: "fail",
        data: data
    });
}