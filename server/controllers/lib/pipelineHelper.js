const moduleHelper = require('./moduleHelper');



// A process is a module plus its corresponding parameters

// Advance the pipeline to the next process and update it
exports.advanceToNextProcess = function(pipeline, cb) {
  var nextProcess;
  var error;

  // The process has not started yet.
  if (!pipeline.currentProcessingModule && pipeline.status == 'To be started') {
    nextProcess = pipeline.process[0];
  }
  // Already started, find index of that process and take the next one
  else {
    currentProcessIndex = pipeline.process.findIndex( function(element) {
      return element.moduleName == pipeline.currentProcessingModule;
    });
    // The pipeline has finished, no more processing left.
    if (pipeline.process.length == currentProcessIndex+1) {
      pipeline.currentProcessingModule = null;
      pipeline.status = 'Finished';
      return cb(null, pipeline);
    }
    else if (currentProcessIndex > -1) {
      nextProcess = pipeline.process[currentProcessIndex+1];
    }
    else {
      error = new Error(`Could not find the module named ${pipeline.currentProcessingModule} in the pipeline`);
      return cb(error, null);
    }
  }
  // We got nextProcess that contains the name of the module
  // And its parameters. We can now call the next module.
  moduleHelper.startModuleProcess(nextProcess, (error) => {
    if (error) { return cb(error, null); }
    // Success: updates pipeline and send it back.
    else {
      pipeline.currentProcessingModule = nextProcess.moduleName;
      pipeline.status = 'In progress';
      return cb(null, pipeline);
    }
  })
}