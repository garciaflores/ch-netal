// ***
// Pipeline Controller file
// ***

const PipelineModel = require('../models/Pipeline.js');

const responseHelper = require('./lib/responseHelper');
const errorHandler = responseHelper.errorHandler;
const successHandler = responseHelper.successHandler;
const failHandler = responseHelper.failHandler; // For eg. "User not found" is a fail, not an error

const pipelineHelper = require('./lib/pipelineHelper');

// DEBUG: fetch all pipelines
exports.debug_pipelines_list = async function(req, res) {
  try {
    const pipelines = await PipelineModel.find({});
    successHandler(res, { pipelines });
  }
  catch (error) {
    errorHandler(res, "debug_pipelines_list: " + error);
  }
}

// Add a new pipeline and start it
exports.pipeline_create = function(req,res) {
  const newPipeline = PipelineModel({
    preProcessingOptions: req.body.preProcessingOptions,
    processes: req.body.processes,
    creationDate: req.body.creationDate,
    description: req.body.description
  });
  newPipeline.save( (error, pipeline) => {
    if (error) { errorHandler(res, error); }
    else {
      successHandler(res, { pipeline: pipeline}, 201);
    }
  });
}
// Get info on an existing pipeline
exports.pipeline_get = function(req, res) {
  const pipelineId = req.params.pipelineId;
  PipelineModel.findById(pipelineId, (error, pipeline) => {
    if (error) { errorHandler(res, error); }
    else if (!pipeline) {
      failHandler(res, { pipeline: `No pipeline was found with id: ${pipelineId}`}, 404);
    }
    else {
      successHandler(res, { pipeline: pipeline });
    }
  })
}
// Delete an existing pipeline
exports.pipeline_delete = function(req, res) {
  const pipelineId = req.params.pipelineId;
  PipelineModel.findByIdAndDelete(pipelineId, (error, pipeline) => {
    if (error) { errorHandler(res, error); }
    else if (!pipeline) {
      failHandler(res, { pipeline: `No pipeline was found with id: ${pipelineId}`}, 404);
    }
    else {
      successHandler(res, { pipeline: null });
    }
  })
}
// Get the status of an existing pipeline
exports.pipeline_status = function(req,res) {
  const pipelineId = req.params.pipelineId;
  PipelineModel.findById(pipelineId, (error, pipeline) => {
    if (error) { errorHandler(res, error); }
    else if (!pipeline) {
      failHandler(res, { pipeline: `No pipeline was found with id: ${pipelineId}`}, 404);
    }
    else {
      successHandler(res, { pipeline: { status: pipeline.status } });
    }
  })
}
