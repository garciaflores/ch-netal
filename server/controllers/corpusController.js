// ***
// Corpus Controller file
// ***

const CorpusModel = require('../models/Corpus.js');

const responseHelper = require('./lib/responseHelper');
const errorHandler = responseHelper.errorHandler;
const successHandler = responseHelper.successHandler;
const failHandler = responseHelper.failHandler; // For eg. "User not found" is a fail, not an error

const path = require('path');
const fs = require('fs');

// DEBUG: find all corpora
exports.debug_corpus_list = async function (req, res) {
  try {
    const corpora = await CorpusModel.find({});
    successHandler(res, { corpora })
  }
  catch (error) {
    errorHandler(res, "debug_corpus_list: " + error);
  }
}
// Add a new corpus
exports.corpus_create = function (req, res) {
  console.log("Demande d'ajout d'un corpus:")
  console.log(req.body);
  const newCorpus = CorpusModel({
    documents: req.body.documents,
    createdBy: req.body.createdBy,
    type: req.body.type,
    creationDate: req.body.creationDate,
    metadata: {
      author: req.body.metadata.author,
      title: req.body.metadata.title,
      description: req.body.metadata.description,
      date: req.body.metadata.date,
      type: req.body.metadata.type,
      size: req.body.metadata.size,
      language: req.body.metadata.language,
      additionalMetadata: req.body.metadata.additionalMetadata,
    }
  });
  newCorpus.save((error, corpus) => {
    if (error) { errorHandler(res, error); }
    else {
      console.log("Successfully added the corpus")
      successHandler(res, { corpus: corpus }, 201);
    }
  })
}
exports.corpus_file_upload = function (req, res) {
  if (!req.files) {
    errorHandler(res, 'corpus_file_upload: No files were sent');
  }
  else {
    let userId = req.body.userId;
    let files = req.files['file[]']
    let resData = []

    let uploadDirectory = path.join(__dirname, '/../uploads/corpora/', userId);

    if (!fs.existsSync(uploadDirectory)) {
      fs.mkdirSync(uploadDirectory);
    }

    let size = 0;

    try {
      if (files.length) {
        files.forEach(file => {
          let filePath = path.join(uploadDirectory, file.name);
          if (fs.existsSync(filePath)) {
            console.log(`File named ${file.name} already exists, replacing it`);
          }
          file.mv(filePath);
          resData.push({ name: file.name, mimetype: file.mimetype, size: file.size, path: filePath });
          size += file.size;
        })
      }
      else {
        let filePath = path.join(uploadDirectory, files.name);
        if (fs.existsSync(filePath)) {
          console.log(`File named ${files.name} already exists, replacing it`);
        }
        // TRY TO WRITE THE BUFFER HERE ?
        files.mv(filePath);
        resData.push({ name: files.name, mimetype: files.mimetype, size: files.size, path: filePath });
        size += files.size;
      }
      successHandler(res, { uploadedFiles: resData, size : size});
    }
    catch (error) {
      errorHandler(res, error);
    }
  }
}
// Get an existing corpus
exports.corpus_get = async function (req, res) {
  const corpusId = req.params.corpusId;
  try {
    const corpus = await CorpusModel.findById(corpusId);
    if (!corpus) {
      errorHandler(res, `corpus_get: No corpus was found with id: ${corpusId}`, 404)
    }
    else {
      successHandler(res, { corpus });
    }
  }
  catch (error) {
    errorHandler(res, error);
  }
}
// Modify an existing corpus
exports.corpus_modify = async function (req, res) {
  const corpusId = req.params.corpusId;
  try {
    const corpus = await CorpusModel.findByIdAndUpdate(corpusId, req.body);
    if (!corpus) {
      errorHandler(res, `corpus_modify: No corpus was found with id: ${corpusId}`, 404);
    }
    else {
      successHandler(res, { corpus });
    }
  }
  catch (error) {
    errorHandler(res, error);
  }
}

// Delete an existing corpus
exports.corpus_delete = async function (req, res) {
  const corpusId = req.params.corpusId;
  try {
    const corpus = await CorpusModel.findByIdAndDelete(corpusId)
    if (!corpus) {
      errorHandler(res, `corpus_delete: No corpus was found with id: ${corpusId}`, 404);
    }
    else {
      successHandler(res, { corpus: null })
    }
  }
  catch (error) {
    errorHandler(res, error);
  }
}

// Get a summary of an existing corpus
exports.corpus_summary = async function (req, res) {
  const corpusId = req.params.corpusId;
  try {
    const corpus = await CorpusModel.findById(corpusId);
    if (!corpus) {
      failHandler(res, { corpus: `No corpus was found with id: ${corpusId}` }, 404)
    }
    else {
      // Almost the same than the model, except we only get the number of docs, and not its content.
      const corpusSummary = {
        corpusId: corpusId,
        createdBy: corpus.createdBy,
        creationDate: corpus.creationDate,
        type: corpus.type,
        documentNumber: corpus.documents.length,
        metadata: corpus.metadata
      };
      successHandler(res, { corpusSummary: corpusSummary });
    }
  }
  catch (error) {
    errorHandler(res, error);
  }
}

// Get conllu of each documents in a corpus
// Merge it ?
exports.corpus_documents_get = async function (req,res) {
  const corpusId = req.params.corpusId;
  try {
    const corpus = await CorpusModel.findById(corpusId);
    if (!corpus) {
      errorHandler(res,`corpus_documents_get: No corpus was found with id: ${corpusId}`, 404);
    }
    else {
      let documentConlluList = []
      corpus.documents.forEach(document => {
        documentConlluList.push(
          { id: document._id, POStags: document.POStags, 
            lemmas: document.lemmas, tokens: document.tokens, tokensIdx: document.tokensIdx}
        );
      })
      successHandler(res, { documentsConlluList: documentConlluList });
    }
  }
  catch (error) {
    errorHandler(res, error);
  }
}

// Get a specific document in a corpus
exports.corpus_document_get = async function (req, res) {
  const corpusId = req.params.corpusId;
  try {
    const corpus = await CorpusModel.findById(corpusId);
    if (!corpus) {
      errorHandler(res, `corpus_document_get: No corpus was found with id: ${corpusId}`, 404);
    }
    else {
      let found = false;
      const documentId = req.params.documentId;
      corpus.documents.forEach(document => {
        if (document.id === documentId) {
          successHandler(res, { corpus: { document: document } });
          found = true;
        }
      });
      if (!found) {
        errorHandler(res, `No document found at this index: ${documentId}`, 404);
      }
    }
  }
  catch (error) {
    errorHandler(res, error);
  }
}
// Change a document in a corpus
// NOT FINISHED
exports.corpus_document_modify = async function (req, res) {
  const corpusId = req.params.corpusId;
  try {
    const corpus = CorpusModel.findById(corpusId);
    if (!corpus) {
      errorHandler(res, `corpus_document_modify: No corpus was found with id: ${corpusId}`, 404);
    }
    else {
      const documentId = req.params.documentId;
      const conlluCol = req.body;
      let conlluColKeys = Object.keys(conlluCol);
      if (!(conlluCol['tokenized'] || conlluCol['POStagged'])) {
        errorHandler(res, "There was an error in the request for corpus_document_modify");
        return;
      }
      let found;
      corpus.documents.forEach(document => {
        if (document.id === documentId) {
          document = Object.assign(document, conlluCol);
          found = true;
          return;
        }
      });
      if (found) {
        try {
          let savedCorpus = await corpus.save();
          successHandler(res, { message: `Successfully modified the document` });
        }
        catch (error) {
          errorHandler(res, error);
        }
      }
      else {
        errorHandler(res, `No document was found with  id: ${documentId}`);
      }
    }
  }
  catch (error) {
    return;
  }
}

// Change a document in a corpus
exports.corpus_document_basic_process_add = async function (req, res) {
  const corpusId = req.params.corpusId;
  const documentId = req.params.documentId;
  const basicProcessConlluCols = req.body;
  try {
    const corpus = await CorpusModel.findById(corpusId);
    if (!corpus) {
      errorHandler(res, `No corpus was found with id: ${corpusId}`, 404);
      return;
    }
    if (!(basicProcessConlluCols['tokens'] || basicProcessConlluCols['POStags'] || basicProcessConlluCols['lemmas'])) {
      errorHandler(res, "There was an error in the request for corpus_document_modify");
      return;
    }
    let isDocumentFound;
    corpus.documents.forEach(document => {
      if (document.id === documentId) {
        document = Object.assign(document, basicProcessConlluCols);
        isDocumentFound = true;
        return;
      }
    });
    if (!isDocumentFound) {
      errorHandler(res, `No document was found with  id: ${documentId}`, 404);
      return;
    }
    let savedCorpus = await corpus.save();
    successHandler(res, { message: `Successfully modified the document` });
  }
  catch (error) {
    errorHandler(res, error);
  }
}

// Get a specific report from a document in a corpus
exports.corpus_document_report_get = function (req, res) {
  const corpusId = req.params.corpusId;
  CorpusModel.findById(corpusId, (error, corpus) => {
    if (error) { errorHandler(res, error); }
    if (!corpus) {
      errorHandler(res, `No corpus was found with id: ${corpusId}`, 404);
    }
    else {
      const documentId = req.params.documentId;

      const document = corpus.documents[documentId];
      if (!document) {
        failHandler(res, { corpus: { document: `No document found at this index: ${documentId}` } }, 404);
      }
      else {
        // Get the report here
        // const report = document.report[...];
        successHandler(res, { corpus: { document: document } });
      }
    }
  })
}
