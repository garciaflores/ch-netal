const express = require('express');
const passport = require('passport');
const cors = require('cors');
const fileUpload = require('express-fileupload');
const auth = require('./routes/auth.js');
const morgan = require('morgan');

const corporaRoutes = require('./routes/corpora.js');
const pipelinesRoutes = require('./routes/pipelines.js');
const corpusProcessesRoutes = require('./routes/corpusProcess.js');
const modulesRoutes = require('./routes/modules.js');
const usersRoutes = require('./routes/users.js')

const swaggerUi = require("swagger-ui-express");
const swaggerJsdoc = require("swagger-jsdoc");
const YAML = require('yamljs');
const path = require('path');
const swaggerDocument = YAML.load(path.join(__dirname, '/config/swagger.yaml'));


// MongoDB

var mongoose = require('mongoose');

// Set up mongoose connection
var mongoDB = 'mongodb://localhost:27017/test'
var mongoose_options = {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false
}
mongoose.connect(mongoDB, mongoose_options);

const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
    console.log('Connection has been made');
})

const app = express();

// Middlewares
app.use(express.json({ limit: "50mb"}));
app.use(morgan('dev'));
app.use(cors());
app.use(fileUpload()); // option: createPArentPath ?
app.use(passport.initialize());
require('./config/passport')(passport)

// Routes
app.get('/', (req, res) => res.send('Hello'));
app.use('/login', auth);
app.use('/corpora', corporaRoutes);
app.use('/pipelines', pipelinesRoutes);
app.use('/corpusProcesses', corpusProcessesRoutes);
app.use('/modules', modulesRoutes);
app.use('/users', usersRoutes);

// const options = {
//   definition: {
//     openapi: "3.0.0",
//     info: {
//     title: "CheneTAL API",
//     version: "0.1"
//     },
//     servers: [
//     {
//         url: "http://localhost:8080/",
//     },
//     ],
//   },
//   apis: ["./routes/*.js", "./models/*.js"]
// };
const options = {
  definition: swaggerDocument,
  apis: ["./routes/*.js", "./models/*.js"]
}
const specs = swaggerJsdoc(options);

app.use(
  "/api-docs",
  swaggerUi.serve,
  swaggerUi.setup(specs, { explorer: true} )
);

const port = process.env.API_PORT || 8081;
app.listen(port, () => console.log(`App listening at http://localhost:${port}`));
