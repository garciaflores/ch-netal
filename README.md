# CheneTAL Project

## Clone the repository

`git clone https://depot.lipn.univ-paris13.fr/garciaflores/ch-netal.git`

`cd ch-netal`

Browse `client` or `server` for their respective installation guide in README.md


## Full start

- Backend
`cd chene-tal/server && npm run start`
- Frontend
`cd chene-tal/client && npm run serve`
- Modules
	- TreeTagger
	`cd chene-tal-modules/tree-tagger && npm run start`
	- Exporter
	`cd chene-tal-modules/exporter && npm run start`
	- Remaining modules starts in the same fashion except for Neoveille which must have _excluded_dico_server.py_ running with :
	`cd chene-tal-modules/neoveille/moduleFiles/lib && python3 excluded_dico_server.py fr it` (for both loading italian and french dictionary for example).
