# Instructions to deploy CheneTAL to the server

I don't really know what I'm doing, this is description of empiric findings.

## Copy repo to the server

`git clone $REPOPATH`

`sudo scp -P 60022 -rv chenetal/ user@tal.lipn.univ-paris13.fr:~`

Move it the /var/www/

## Client configuration

Build the client:
`npm run build`

In `vue.config.js`, add the following:
`publicPath: process.env.NODE_ENV === 'production'? '/chenetal/' : '/',`

## Set up Apache

Install apache2:
`sudo apt install apache2`

Change stuff in the file _/etc/apache2/sites-available/000-default.conf_:
`DocumentRoot /var/www/html`
becomes
`DocumentRoot /var/www/chenetal/client/dist`

Check that apache2 is running (and enabled):
`systemctl status apache2`

Go check _https://tal.lipn.univ-paris13.fr/chenetal/_




## Install and run mongo

Follows the installation guidelines from https://docs.mongodb.com/manual/installation/

## Server configuration

I have no idea

## Notes from Jaime meeting

Check pm2 https://pm2.keymetrics.io/docs/usage/quick-start/

Configuration file of pm2 for invited_professors:
https://depot.lipn.univ-paris13.fr/etamine/plugins/invited_professors/-/blob/master/server/ecosystem.config.js

