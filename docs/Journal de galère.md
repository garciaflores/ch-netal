# Journal de galère pour mettre en place une démo de l'outil linguistique Chêne TAL sur le serveur TAL

*Jorge Garcia Flores, août 2021*


## 1. Installation en local avec nginx
```bash
$ sudo service apache2 stop
```
#### Téléchargement du code de Quentin et installation d'après la doc
Le code source est sur le git-lab: https://depot.lipn.univ-paris13.fr/garciaflores/ch-netal

J'ai suivi la documentation des `README.md`pour l'installation et du client et et du serveur 

#### Installation du serveur

```bash
$ git clone git@depot.lipn.univ-paris13.fr:garciaflores/ch-netal.git
$ cd cd ch-netal/server
$ npm install
$ npm start

> chenetal-server@1.0.0 start /home/garciaflores/code/javascript/ch-netal/server
> node server.js

App listening at http://localhost:8081
```

La doc du serveur spécifie qu'il faut faire un `POST` à partir de la ligne de commande pour enregistrer chaque module du répertoire `ch-ntal/server/files`, spécifié sous la forme d'un fichier `.json`

```bash
$ curl -X POST -H "Content-Type: application/json" -d @Morfetik_Complex.json http://localhost:8081/modules
{"status":"success","data":{"newModule":{"_id":"612f3addbfc3ea588c5ed76d","parameters":[],"__v":0}}}
```

#### installation du client

```bash
$ cd ch-netal/client
$ npm install
$ npm run serve
 DONE  Compiled successfully in 25405ms                                                                                                                                                               10:50:23


  App running at:
  - Local:   http://localhost:8080/ 
  - Network: http://10.10.0.218:8080/

  Note that the development build is not optimized.
  To create a production build, run npm run build.

```
Si on ouvre le navigateur web à l'adresse `http://localhost:8080` on doit voir l'ecran d'accueil ChêneTAL

![image-20210901114105972](Journal de galère.assets/image-20210901114105972.png)

## 2. Installation de Chêne TAL sur le serveur TAL 

### 2.1. Vérifier que l'adresse https://tal.lipn.univ-paris13.fr/chenetal est bien configurée sur tal-proxy

D'abord je me suis connecté sur `tal-proxy` et j'ai vérifié que sur le fichier `/etc/apache2/web.proxy` le *endpoint* **chenetal** est bien cofiguré

```bash
#Quentin - Jorge
ProxyPass /chenetal http://192.168.69.54/
ProxyPassReverse /chenetal http://192.168.69.54/
```

Après j'ai lancé le serveur web *nginx* dans `tal-chenetal`

```bash
$ sudo service nginx start
```

Et enfin j'arrive à obtenir la page statique d'Apache (*"It works"*) à l'adresse https://tal.lipn.univ-paris13.fr/chenetal (sur le serveur cette page est dans le répertoire `/var/www/html`)

### 2.2 Installation de  ChêneTAL sur le serveur TAL

On ne peux pas faire un `git clone` à partir de https://depot.lipn.univ-paris13.fr/ sur le serveur TAL (ToDo: comment on peut faire pour avoir un mirroir du git-lab quelque par d'accessible depuis `tal-chenetal`? )

Du coup, il faut faire le clone local et après recopier tout avec `cp`où `rsync`sur le serveur TAL. 

Pour résumer : j'ai recopié le contenu le plus récent du depôt gitlab Chênetal https://depot.lipn.univ-paris13.fr/garciaflores/ch-netal/ sur le répertoire `/var/www/ch-netal/`de la machine virtuelle `tal-chenetal`. 

Par le moment, la seule solution que je vois dans l'horizon est d'avoir un autre git-lab exclusif pour la plateforme TAL (mais c'est une solution un peu absurde). 

Sur  `tal-chenetal:/var/www/ch-netal/`je répète l'installation de la [section 1.](##1.-installation-en-local-avec-nginx) Cependant, pour déployer le serveur en production, à la place de `npm run devstart`il faut faire `sudo npm run build`. Cette commande va déployer le cleint sur le répertoire `/var/www/ch-netal/client/dist` du serveur ChêneTAL. Il faut réfaire les commandes `curl` spécifiées sur la section 1 pour déclarer les modules définis dans les fichiers .json  (section 1)

Maintenant il faut configurer le client pour qu'il réponde à l'adresse https://tal.lipn.univ-paris13.fr/chenetal

Pour ce faire, je vais d'abord déclarer le répertoire où je viens de déployer sur la config de Nginx, fichier `/etc/nginx/sites-available/default`

```json
server {
    listen      80 default_server;
    listen      [::]:80 default_server;
    root    /var/www/ch-netal/client/dist;
    server_name _;
    charset utf-8;
    location / {
        root /var/www/ch-netal/client/dist;
        try_files $uri  /index.html;
    }        
```

Il faut également paramétrer la variable publicPathde Vue sur le fichier `/var/www/ch-netal/client/vue.config.js`

```json
module.exports = {
	"transpileDependencies": [     
	"vuetify"   
	],   
	devServer: {
        port:8080   
    },   
    pluginOptions: {
        i18n: {
            locale: 'fr',
            fallbackLocale: 'en',
            localeDir: 'locales',
            enableInSFC: true     
        }   
    },
    publicPath: '/chenetal/'
}
```

Après ces modifs, il faut ré initier nginx

```bash
$ sudo systemctl restart nginx
```

Maintenant, à l'adresse https://tal.lipn.univ-paris13.fr/chenetal on doit voir la page suivante:

![image-20210913161214512](Journal de galère.assets/image-20210913161214512.png)



Par contre, lorsqu'on clique sur l'onglet  **PIPELINE**, on a une erreur 

![image-20210913161820548](Journal de galère.assets/image-20210913161820548.png)

![image-20210913161743000](Journal de galère.assets/image-20210913161743000.png)

Donc maintenant il faut définir un *Proxy Pass* sur nginx pour l'adresse `https://tal.lipn.univ-paris13.fr/chenetal/api`vers le serveur en s'inspirant et de la [config du module de publications de l'intranet faite en Apache](https://depot.lipn.univ-paris13.fr/etamine/tools/apache/-/blob/master/apache.conf) et vérifier que la variable d'environnement `VUE_APP_API_URL` pointe bien vers le serveur, comme dans le cas du [module de publications de l'intranet](https://depot.lipn.univ-paris13.fr/garciaflores/ch-netal/-/blob/master/client/src/plugins/axios.js). Sur ChêneTAL, cette config est défini dans le fichier [axios.js](https://depot.lipn.univ-paris13.fr/garciaflores/ch-netal/-/blob/master/client/src/plugins/axios.js).

ToDo: Configurer le ProxyPass en suivant cette doc: https://docs.nginx.com/nginx/admin-guide/web-server/reverse-proxy/



