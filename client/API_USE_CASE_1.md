# Cas d'utilisation 1

## Version Simple

Corpus Voix du Nord -> Néoveille -> SDMC

## Version optimisée

Corpus Voix du Nord -> TreeTagger -> Néoveille -> Filtrage sur les phrases contenant les néologismes -> SDMC

## Détails des appels API

### Version simple

#### POST /corpus

Paramètres : Le corpus sera envoyé en paramètre ainsi que les autres paramètres.
``` javascript
{
    corpus_content:
    [
        'Article1 de la voix du Nord',
        'Article2 de la voix du Nord',
        'Article3 de la voix du Nord'
    ],
    corpus_parameters: {
        lang: 'fr',
        author: 'La voix du Nord',
        date: null
    }
}
```

On envoie le corpus à la BDD
La BDD nous renvoie un id correspondant qui sera utilisé pour faire le lien entre le traitement et le corpus.
À titre d'exemple, on imagine que l'id retourné est: __1DT4G__

#### POST /chaine

Paramètres :
``` javascript
{
    corpus_id: '1DT4G',
    treatment_chain: [
        {
            module_name: 'Néoveille',
            parameters:
            [
                ...
            ]
        },
        {
            module_name: 'SDMC',
            parameters:
            [
                {
                    parameter_name: 'motif_type',
                    value: 'Forme du mot seul'
                },
                {
                    parameter_name: 'motif_representation',
                    value: 'Fermés (sans sous-motifs de même fréquence)'
                },
                {
                    parameter_name: 'gap',
                    value: ["0","0"]
                },
                ...
            ]
        }
    ]
}
```

Ainsi, le dispatcher saura quel corpus traiter et possèdera aussi la liste des modules concernés dans le traitement ainsi que leur ordre et leurs paramètres.

#### POST /Neoveille/treatment

``` javascript
{
    corpus_id: '1DT4G',
    parameters: {
        // Parameters for Neoveille
    }

}
```

Cela va activer le module Néoveille afin qu'il effectue le traitement.
Tout d'abord, il doit récupérer le fichier corpus.

#### GET /corpus/1DT4G

Retourne le corpus

#### PUT /corpus/1DT4G

``` javascript

// Envoi de l'output de Néoveille à la BDD, ainsi que maj du corpus

```


* Retourne ensuite au dispatcher

#### POST /SDMC/treatment

``` javascript
{
    corpus_id: '1DT4G',
    parameters: {
        // Parameters for SDMC
    }
}
```

#### GET /corpus/1DT4G

Retourn le corpus



#### PUT /corpus/1DT4G

``` javascript

// Envoi de l'output de Néoveille à la BDD, ainsi que maj du corpus

```

* Retourne ensuite au dispatcher qui s'aperçoit que le traitement est alors terminé

GET /chaine/status/1DT4G renvoie alors 'finished'


Dans son profil, il y aura comme appel

### GET /corpus/user/MY_USER

(besoin d'un résumé juste ?)

Puis il pourra cliquer dessus pour accéder à l'outil de visualisation

### GET /corpus/1DT4G