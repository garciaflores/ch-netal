# chene-tal

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

## TODO

### Define corpus

* Prévoir une taille maximum pour les fichiers en input
* Pouvoir supprimer les fichiers selons certains conditions (utilisateur n'apas été jusqu'au bout de l'UX, au bout de X jours pour un utilisateur anonyme)

### Define pipeline

* Enlever un module quand il est déjà selectionné dans la timeline ? (peut-on avoir plusieurs fois un module dans une pipeline?)
* Prévoir les modules affichés en fonction de la langue du corpus

### Prevalidation

* Revoir la présentation globale

### Validation

* Checker si utilisateur connecté ou non
* Si non connecté, donner un lien à consulter pour ses résultats.

### Display Results (Visualization tool)

* Voir comment mieux gérer le redimensionnement -> Check ?
* Faire une transition pour afficher ou cacher le paneau de droite
* Checker pour faire des titres (Paneau d'annotation, Panneau de document...)
- Panneau de droite : Chaque étiquette doit avoir une couleur

### Others

* Implementer Vuex ? Fait mais non utilisé
* Implementer i18n pour la localization
* Detecting user locale

## QUESTIONS

* Multiple page form: devrais-je utiliser Vuex? Stocker les paramètres dans la session ?
Besoin de garder momentanément potentiellement un fichier lourd.
* Comment gérer le json des paramètres des modules et la traduction ? (import a file, cfshared locale msg in i18n)

