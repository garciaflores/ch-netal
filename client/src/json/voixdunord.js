exports.result = [
{
    id: 1,
    name: 'Documents source :',
    children: [
      { id: 2, name: 'Fichier1',
        content: {
            title:"Nuits secrètes d’Aulnoye-Aymeries: Deux nouveaux noms et la répartition des scènes!",
            contents:"<SDMC>Nuits secrètes</span> d’<NAMED_ENTITY_PLACE>Aulnoye-Aymeries</span>: Deux nouveaux noms et la <SMDC>répartition des scènes</span>!\n\
            L'actualité en Nord - Pas-de-Calais\n\n<SDMC>Nuits secrètes</span> d’<NAMED_ENTITY_PLACE>Aulnoye-Aymeries</span>: \
            Deux nouveaux noms et la <SDC>répartition des scènes</span>!\nLe Festival Les <SDMC>Nuits Secrètes</span> s’enrichit de deux nouveaux noms \
            rendus publics dès vendredi midi. Citons <NAMED_ENTITY_NAME>Mr. Oizo</span> déjà présent il y a trois ans et <NAMED_ENTITY_NAME><NEOVEILLE>The Oblivians</span></span>. \
            Quant au Grand Parcours, c’est complet.\n<NAMED_ENTITY_NAME>Mr Oizo</span> avait déjà <SDMC>créé l’événement</span> au Jardin en 2013. \
            Il revient aux <SDMC>Nuits Secrètes</span>. PHOTO ARCHIVES LA VOIX\nC’est le principe que d’égrener au fil de l’eau la programmation du festival <NEOVEILLE>aulnésien</span> prévu les 29, 30 et 31 juillet. \
            Les organisateurs des <SDMC>Nuits Secrètes</span> ne changent pas une formule qui marche. \
            Dès ce vendredi midi sur le site, ils annonçent deux nouveaux noms. <NAMED_ENTITY_NAMED>Mr Oizo</span> et <NEOVEILLE>The Oblivians</span>. Un trio punk – composé de Greg Cartwright, Jack Yarber et Eric Friedl – \
            formé à <NAMED_ENTITY_PLACE>Memphis</span> dans les années 90. Il y aura des bas résilles déchirés et de la crête dans les rues d’Aulnoye-Aymeries, \
            cette année, le dimanche.\nRevenons à Mr Oizo. Oiseau rare par rapport à d’autres habitués des grandes messes comme Vitalic (programmé le dimanche).\nIl avait déjà séduit le public de la scène du Jardin en 2013. \
            Et revient donc... au Jardin le samedi 30 juillet. Voilà qui réjouira les connaisseurs et les autres, ceux qui apprécient cet artiste sous ses autres facettes. \
            Quentin Dupieux de son vrai nom est aussi réalisateur de films. Il s’est musicalement fait connaître à travers son single Flat Beat et sa peluche jaune qui avait fait le buzz.\n\
            Il fait actuellement son grand retour en musique avec son EP Hand in Fire. Le plus abouti selon lui, sur lequel apparaît la chanteuse britannique Charli XCX. \
            Ça promet !\nOlivier Connan, le directeur artistique, aime bien faire revenir aux <SDMC>Nuits Secrètes</span> les artistes qui ont su créer une ambiance au Jardin. \
            C’est le cas.\nOutre ces deux noms d’artistes rejoignant la cohorte de ceux déjà connus, Selah Sue, Alice on the roof notamment, c’est toute la répartition des scènes (lire ci dessous) que l’organisation dévoile.\
            Avec le Grand Parcours déjà complet. Y aurait-il des paris sur une présence Souchon-Voulzy sur ce parcours ? \
            On peut l’imaginer...\nCa y est, la répartition des artistes programmés sur le festival est tombée !\n\
            Vendredi 29 juillet\nGrande scène\nAlice on the roof, Lilly wood & The Prick, Selah Sue (en photo), The shoes."
        },
        link: '/files/article1.conll'
      },
      { id: 3, name: 'Fichier2',
        content: {
            title:"Neufchâtel-Hardelot : l’opposition accuse de le maire de «déni de démocratie»",
            contents:"Neufchâtel-Hardelot : l’opposition accuse de le maire de \"déni de démocratie\"\nL'actualité en Nord - Pas-de-Calais\n\nNeufchâtel-Hardelot : l’opposition accuse de le maire de \"déni de démocratie\"\nLes quatre élus de l’opposition ont refusé de voter le budget jeudi soir. Ils ont même quitté la salle après avoir lu une déclaration. Ils reprochent au maire leur \" mise à l’écart des travaux préparatoires concernant les finances locales \".\nJean-Pierre Pont, maire, et son premier adjoint Daniel Fauquet ont eu beau claironner que \"\nc’est un beau budget, sans augmentation des impôts locaux, mais avec beaucoup d’investissements prévus\n\", rien n’y a fait, l’opposition ne l’a pas voté.\nSes quatre membres ont quitté la salle du conseil après avoir reproché au maire dans une déclaration \"\nsa persistance à refuser la mise en place d'une commission des finances\n\", désapprouvant au passage \"\nsa conception très personnelle de la démocratie locale\n\".\n\" Déni de démocratie \"\nAvant de conclure : \"\nCette situation peut être analysée comme un déni de démocratie ainsiqu’une insulte aux 39 % d’électeurs qui nous ont fait confiance lors des élections.\n\"\nJean-Pierre Pont a répliqué : \"\nla démocratie, c’est se battre, mes conseillers peuvent en témoigner, je suis loin d’être un dictateur\n\". Les débats se sont poursuivis dans une drôle d’ambiance."
        },
        link: '/files/article2.conll'
      },
      { id: 4, name: 'Fichier3',
        content: {
          title:"Béthune : plus que ce soir pour céder à « L’Annonce faite à Marie »",
          contents:"Béthune : plus que ce soir pour céder à \" L’Annonce faite à Marie \"\nL'actualité en Nord - Pas-de-Calais\n\nBéthune : plus que ce soir pour céder à \" L’Annonce faite à Marie \"\nEncensée depuis deux ans par la critique, la pièce de Paul Claudel, mise en scène par Yves Beaunesne, est à l’affiche de la Comédie de Béthune. Plus qu’une seule représentation, ce vendredi soir, pour comprendre les raisons d’un tel succès. Jeudi soir, dans la salle, on a croisé un spectateur pas comme les autres : François Claudel, petit-fils de...\nCher François Claudel, j’ai eu bien tort de m’inquiéter. La grippe revancharde qui s’accroche au printemps ne vous guettait pas sournoisement au coin du beffroi. Si en deux heures et demie, hôte d’exception du Palace, vous avez plongé à tâtons dans vos poches, ce n’était pas le fait d’un virus, mais juste de l’émotion. \"\nQuand je vois les larmes, c’est que le baromètre a fonctionné !\n\" Jeudi soir, vous avez enjambé deux années d’un coup d’un seul, quand à Paris vous aviez assisté aux premières de L’Annonce faite à Marie dans la mise en scène d’Yves Beaunesne. Pas trop inquiet : l’homme connaît le verbe de votre grand-père comme sa poche, qui a choisi la troisième version du texte pour aller chercher derrière le poète réputé catho, la part d’hérétique qui interroge \"\nle dialogue de la chair et de la tentation\n\" ; derrière la lépreuse devenue sainte par un miracle de Noël, la part de divin descendue dans l’humain.\n\" Je suis las d’être heureux \"\nDeux ans plus tard, à Béthune, \"\nc’est encore mieux qu’à Paris !\n\" et c’est bien là tout l’art du théâtre, vivant et en mouvement perpétuel. Jean-Claude Drouot parle d’une liberté gagnée d’avoir apprivoisé le texte et de s’en être laissé posséder. Lui, le père, le patriarche à la voix puissante, campé sur ses jambes solides comme l’arbre dans la terre, est \"\nlas d’être heureux\n\". Il entend l’appel de Jérusalem, comme Violaine, sa fille pure, embrasse la lèpre en accordant le baiser du pardon à Pierre de Craon. Jean-Claude, Fabienne, Marine, Damien, Thomas... vous ont tiré des larmes, François, presque autant que Judith. Judith, Violaine, la lépreuse, fragile silhouette drapée, actrice au service d’un texte puissant, émouvante entre tout dans la déchéance vers le tombeau, poussée par sa sœur, Mara, \"\ncelle qui tire les ficelles\n\", Marine Sylf à la voix d’or.\nDes acteurs à la voix d’or\nParce que sûrement aussi, François, c’est la musique qui a achevé de vous chavirer le cœur. Grand-papa Paul la rêvait ainsi, cette pièce remaniée pendant 50 ans de sa vie, son \"\nopéra de paroles\n\" comme il disait. Des décors, pour quoi faire ? Un rideau de fils suffit, qui accroche la lumière et joue avec les clairs-obscurs, parfois, on jurerait une toile des Le Nain. Le génie d’Yves Beaunesne, c’est d’avoir confié la partition musicale à Camille Rocailleux et à deux violoncelles qui, paraît-il, sont les plus proches de la tessiture humaine. Les acteurs chantent et c’est magique, élans mystiques qui culminent avec Violaine s’effondrant comme en une descente de croix, d’ailleurs la langue, magnifique, tiens, c’est de l’araméen...\nDans votre cartable, François, l’épais dossier de presse qui ne vous quitte pas, bourré d’éloges, résume tout le bien de cette mise en scène. Et si elle parle tant au public, c’est peut-être qu’elle a traversé le siècle, c’est encore plus flagrant sous les costumes contemporains des acteurs. C’est vous qui le dites : \"\nElle est un témoignage de l’époque, la lèpre qui évoque le SIDA, les gens à nos portes que nous refusons...\n\" Et derrière l’appel qui vous fait tout abandonner, la peur de l’intégrisme. Un \"\nsens de l’âme humaine\n\" qui valait bien quelques mouchoirs.\nCe vendredi 1er avril à 20 h, au Palace, rue du 11-Novembre à Béthune. De 6 à 20 euros. Réservations au 03 21 63 29 19."
        },
        link: '/files/article3.conll'
      },
    ],
},
{
    id: 5,
    name: 'Néoveille :',
    children: [
      {
        id: 6,
        name: 'Résultats de Néoveille',
        content: {
            title: "Néoveille Output",
            contents:"y a trois ans et {The Oblivians} | count=2\n\
            eau la programmation du festival {aulnésien} prévu les 29, 30 et | count=1\n\
            Mr Oizo et {The Oblivians} | count=2"
        },
        link: '/files/neoveille.txt'
      }
    ],
},
{
    id: 15,
    name: 'SDMC :',
    children: [
      { id: 16, name: 'Résultats de SDMC',
        content: {
            title: "SDMC Output",
            contents:"{Nuits}{secrètes} sup=6\n\
            {créé}{l'}{évènement} sup=1"
        },
        link:'/files/SDMC.txt'
    }
    ],
}];