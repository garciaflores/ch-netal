import axios from "@/plugins/axios";

const state = {
  token: localStorage.getItem("token") || null,
  user: JSON.parse(localStorage.getItem("user")) || null
};

const mutations = {
  SET_TOKEN (state, token) {
    state.token = token;
  },
  SET_USER (state, user) {
    state.user = user;
  }
};

const actions = {
  login ({ commit }, payload) {
    return axios.post("/login", payload).then(res => {
      const { token, user } = res.data;
      localStorage.setItem("token", token);
      localStorage.setItem("user", JSON.stringify(user));
      commit("SET_TOKEN", token);
      commit("SET_USER", user);
    });
  },
  logout ({ commit }) {
    return new Promise(resolve => {
      localStorage.removeItem("token");
      localStorage.removeItem("user");
      commit("SET_TOKEN", null);
      commit("SET_USER", null);
      resolve();
    });
  }
};

const getters = {
  token: state => state.token,
  user: state => state.user
};

const loginModule = {
  state,
  mutations,
  actions,
  getters
};

export default loginModule;
