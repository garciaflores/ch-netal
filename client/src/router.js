// Corpus Processing
import Corpus from '@/components/DefineCorpus/DefineCorpus';
import Pipeline from '@/components/DefinePipeline/DefinePipeline';
import PreValidation from '@/components/PreValidation/PreValidation';
import CorpusProcessValidation from '@/components/CorpusProcessValidation';

// User pages
import MyProfile from '@/components/UserPages/MyProfile';
import TreatmentList from '@/components/UserPages/TreatmentList/TreatmentList';
import ProfileCorporaList from '@/components/UserPages/ProfileCorporaList/ProfileCorporaList';
import AddProfileCorpus from '@/components/UserPages/AddProfileCorpus';

// Corpus Process pages
import CheckCorpusProcess from '@/components/CheckCorpusProcess/CheckCorpusProcess';

// Visualisation tool
import DisplayResults from '@/components/DisplayResults/DisplayResults';

import Login from '@/components/Login';
import NotFound from '@/components/NotFound';


import VueRouter from 'vue-router';
import Vue from 'vue';
const routes = [
  { path: '/', name: 'Corpus', component: Corpus },
  { path: '/pipeline', name: 'Pipeline', component: Pipeline},
  { path: '/validation', name: 'CorpusProcess', component: CorpusProcessValidation},
  { path: '/user/list', name: 'TreatmentList', component: TreatmentList },
  { path: '/user/profile-corpora', name: 'ProfileCorporaList', component: ProfileCorporaList},
  { path: '/user/add-corpus', name: 'AddProfileCorpus', component: AddProfileCorpus},
  { path: '/corpusprocess/:corpusprocessid', name: 'CheckCorpusProcess', component: CheckCorpusProcess},
  { path: '/id/results', name: 'DisplayResults', component: DisplayResults },
  { path: '/profile', name: 'MyProfile', component: MyProfile },
  { path: '/prevalidation', name:'PreValidation', component: PreValidation },
  { path: '/login', name:'Login', component: Login },
  { path: '/*', name: 'NotFound', component: NotFound}
]

Vue.use(VueRouter);

const router = new VueRouter({
  routes
});

export default router;