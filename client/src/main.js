import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify';
import router from './router';
import i18n from './plugins/i18n';
import store from './store';
import FlagIcon from 'vue-flag-icon';

Vue.config.productionTip = false
Vue.use(FlagIcon);

new Vue({
  vuetify,
  router,
  i18n,
  store,
  render: h => h(App)
}).$mount('#app')
